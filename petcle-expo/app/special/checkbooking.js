import _ from "lodash";
import moment  from "moment";

const elocker_id = 17;
const nganxep = 5; //cm

class checkBooking {
  constructor(databooking) {
    this.data = databooking;
    this.checkLockerBooking = new checkLockerBooking(this.data.locker_booking);
  }
  checkin() {
    let checkin = _.get(this.data, "checkin_datetime");
    return true;
  }
  checkout() {
    let checkout = _.get(this.data, "checkout_datetime");
    // if(checkout < this.now)
    //     return false;
    return true;
  }
  checkCreateTime() {
    let createTime = _.get(this.data, "create_datetime");
    let now = new Date().getTime();
    let delayTime = now - createTime;
    // if(delayTime > 3) // 3s
    //     return false;
    return true;
  }
  checkStatusId() {
    let booking_status_id = _.get(this.data, "booking_status_id");
    return true;
  }
  resultsCheckCorrect() {
    return (
      this.checkLockerBooking.resultsCheckCorrect()
      && this.checkCreateTime()
      && this.checkin()
      && this.checkout()
      && this.checkStatusId()
    );
  }
}

class checkLockerBooking {
  constructor(datalockerbooking) {
    this.data = datalockerbooking;    
  }
  checkType() {
    let locker_index_end = _.get(this.data, "checkin_datetime");
    let locker_index_start = _.get(this.data, "locker_index_start");
    let size = (locker_index_end - locker_index_start) * nganxep;
    return true;
  }
  resultsCheckCorrect() {
    return this.checkType();
  }
}

export class checkCorrectUserBooking {
  constructor(databooking) {
    this.data = databooking;
    this.checkBooking = new checkBooking(this.data.data, this.now);
  }
  checkEmitTime() {
    let emit_datetime = _.get(this.data, "emit_datetime");
    let now = new Date().getTime();
    console.log(now, emit_datetime)

    let delayTime = now - emit_datetime;
    console.log(delayTime)
    if (delayTime > 3000)
      return false;
    return true;
  }
  resultsCheck() {
    return (
      this.checkBooking.resultsCheckCorrect()
      // && this.checkEmitTime()
    );
  }
}