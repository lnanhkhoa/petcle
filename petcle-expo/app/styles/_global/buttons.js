import { Platform, StyleSheet } from 'react-native';
import { fonts } from '../fonts'
export default StyleSheet.create({
    buttonClassic: {
        width: 325,
        height: 55,
        borderRadius: 2,
        backgroundColor: "rgb(88, 18, 71)"
    },
    //#region [Button Login]
    buttonLogin: {
        width: 325,
        height: 55,
        borderRadius: 40,
        borderStyle: "solid",
        borderWidth: 0.5,
        borderColor: "rgb(255, 255, 255)",
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 20,
        paddingLeft: 30,
    },
    buttonLoginText: {
        fontSize: 15,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        color: "rgb(255, 255, 255)",
        lineHeight: 20,
        marginLeft: 30,
    },
    buttonLoginIcon: {
        width: 25,
        color: '#FFF'
    }
    //#endregion
});