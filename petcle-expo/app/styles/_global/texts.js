
import { Platform, StyleSheet } from 'react-native';
import { fonts } from '../fonts'
let texts = StyleSheet.create({
    p: {
        color: 'black',
        fontFamily: fonts.OpenSans,
        fontSize: 14,
    },
    title: {
        fontWeight: 'bold',
        color: 'black',
        fontFamily: fonts.OpenSans,
        fontSize: 20,
    },
    subTitle: {
        fontWeight: 'bold',
        color: 'black',
        fontFamily: fonts.OpenSans,
        fontSize: 14,
    },
    data: {
        color: 'black',
        fontFamily: fonts.OpenSans,
        fontSize: 25,
    },

});
export default { texts }




