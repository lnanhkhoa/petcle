import { Platform, StyleSheet } from 'react-native';
let _global = {
    linear_gradient_main: {
        start: { x: 0.0, y: 0.0 },
        end: { x: 1.0, y: 1.0 },
        locations: [0, 1],
        colors: ['#2F0042', '#7C0F00'],
    },
    linear_gradient_navigation: {
        start: { x: 0.0, y: 0.0 },
        end: { x: 1.0, y: 1.0 },
        locations: [0, 1],
        colors: ['#550853', '#693E0B']
    },
    main : StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#F5FCFF',
        },
        welcome: {
            fontSize: 20,
            textAlign: 'center',
            margin: 10,
        },
        instructions: {
            textAlign: 'center',
            color: '#333333',
            marginBottom: 5,
        },
        linearGradientMainStyle: {
            flex: 1
        },
    })
}
export default _global 