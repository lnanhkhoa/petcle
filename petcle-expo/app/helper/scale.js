import { Dimensions } from 'react-native';
const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 1280;
const guidelineBaseHeight = 800;

const zeplinBaseWidth = 741;
const zeplinBaseHeight = 519;


export const scale = size => width / guidelineBaseWidth * size;
export const scaleVertical = size => height / guidelineBaseHeight * size;
export const scaleModerate = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;
export const scaleDesign = size => width / zeplinBaseWidth * size;
export const scaleDesignVertical = size => height / zeplinBaseHeight * size;