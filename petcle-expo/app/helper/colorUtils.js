import { Platform, StatusBar, Animated } from 'react-native';
import color from 'tinycolor2';

export function hexToRGB(hex) {
  hex = parseInt(hex.slice(1), 16);
  let r = hex >> 16;
  let g = hex >> 8 & 0xFF;
  let b = hex & 0xFF;
  return `rgb(${r},${g},${b})`
};
export function hexToRGBA(hex, opacity) {
  opacity = opacity ? opacity : 1;
  hex = parseInt(hex.slice(1), 16);
  let r = hex >> 16;
  let g = hex >> 8 & 0xFF;
  let b = hex & 0xFF;
  return `rgba(${r},${g},${b},${opacity})`
};
export function setStatusBarStyle(backgroundColor) {
  function chooseBarStyle(bgColor) {
    return color(bgColor).isDark() ? 'light-content' : 'default';
  }

  function setStyle(bgColor) {
    if (Platform.OS === 'android') {
      StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.2)');
    } else {
      const barStyle = chooseBarStyle(bgColor);
      StatusBar.setBarStyle(barStyle);
    }
  }
  if (backgroundColor && backgroundColor._parent instanceof Animated.Value) {
    backgroundColor._parent.addListener((animation) => {
      setStyle(backgroundColor._interpolation(animation.value));
    });
    setStyle(backgroundColor._interpolation(0));
  } else {
    setStyle(backgroundColor);
  }
}