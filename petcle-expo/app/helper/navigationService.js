// navigationService.js

import { NavigationActions, StackActions } from 'react-navigation';
const _navigator = undefined;

function navigateReset(route, params){
  return StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({
        routeName: route,
        params: params
      })
    ]
  });
}

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function dispatchReset(routeName, params){
  _navigator.dispatch(navigateReset(routeName, params))
}


// add other navigation functions that you need and export them

export default {
  navigate,
  setTopLevelNavigator,
  dispatchReset
};