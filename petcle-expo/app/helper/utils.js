export default {
    meterToString : (value) => {
        if (!value)
            return `--`;
        if (value > 1000)
            return `${(value / 1000).toFixed(1)} km`;
        return `${value.toFixed(1)} m`;
    }
}