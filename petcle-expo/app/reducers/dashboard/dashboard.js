import _ from 'lodash';
import { act_hardware } from '../../actions/';
import { define } from '../../services/hardware_api/hardware/';


const resDefault = {
  hardware: {
    readline: {
      string: ''
    }
  }
}

export const dashboardReducers = (dashboard = resDefault, action) => {
  switch (action.type) {
    case act_hardware.action.RESPONSE.RAW_DATA:
      _.set(dashboard, ['hardware', 'readline', 'string'], action.data);
      break;
    default:
      // do nothing
      return dashboard;
  }


  return { ...dashboard }
}