import * as ble from "../actions/ble/bluetooth.js";
import _ from "lodash";

let defaultState = {
  devices: {},
  selectedDeviceUUID: null,
  selectedServiceUUID: null,
  selectedCharacteristicUUID: null,
  scanning: false,
  errors: [],
  state: ble.action.DEVICE_STATE_DISCONNECTED,
  operations: {},
  transactionId: 0
};

function bleReducers (state = defaultState, action) {
  switch (action.type) {
    case ble.action.START_SCAN:
    state.scanning = true;
    break;
    // case ble.action.STOP_SCAN:
    //   _.set(state, "scanning", false);
    //   break;
    // case ble.action.DEVICE_FOUND:
    //   let resultsMerge = _.merge(
    //     _.get(state, ["devices", action.device.id]),
    //     action.device
    //   );
    //   _.set(state, ["devices", action.device.id], resultsMerge);
    //   break;
    // case ble.action.CHANGE_DEVICE_STATE:
    //   _.set(
    //     _.set(_.set(state, "scanning", false), "state", action.state),
    //     "selectedDeviceUUID",
    //     action.deviceIdentifier
    //   );
    //   break;
    // case ble.action.UPDATE_SERVICES:
    //   let resultsMerge1 = _.merge(
    //     _.get(state, ["devices", action.deviceIdentifier, "services"]),
    //     action.services
    //   );
    //   _.set(
    //     state,
    //     ["devices", action.deviceIdentifier, "services"],
    //     resultsMerge1
    //   );
    //   break;
    case ble.action.UPDATE_CHARACTERISTIC:
    let resultsMerge2 = _.merge(
        _.get(state, [
          "devices",
          action.deviceIdentifier,
          "services",
          action.serviceUUID,
          "characteristics",
          action.characteristicUUID
          ]),
        action.services
        );
    _.set(
        state,
        [   "devices", action.deviceIdentifier,
            "services", action.serviceUUID,
            "characteristics", action.characteristicUUID ],
        resultsMerge2
        );
    break;

    // case ble.action.SELECT_SERVICE:
    //   _.set(state, "selectedServiceUUID", action.serviceUUID);
    //   break;
    // case ble.action.SELECT_CHARACTERISTIC:
    //   _.set(state, "selectedCharacteristicUUID", action.characteristicUUID);
    //   break;
    // case ble.action.WRITE_CHARACTERISTIC:
    //   let results = _.set(state, ["operations", transactionId], {
    //     type: "write",
    //     state: "new",
    //     deviceIdentifier: action.deviceIdentifier,
    //     serviceUUID: action.serviceUUID,
    //     characteristicUUID: action.characteristicUUID,
    //     base64Value: action.base64Value,
    //     transactionId
    //   });
    //   _.set(results, "transactionId", transactionId + 1);
    //   break;
    // case ble.action.READ_CHARACTERISTIC:
    //   let results1 = _.set(state, ["operations", transactionId], {
    //     type: "read",
    //     state: "new",
    //     deviceIdentifier: action.deviceIdentifier,
    //     serviceUUID: action.serviceUUID,
    //     characteristicUUID: action.characteristicUUID,
    //     transactionId
    //   });
    //   _.set(results1, "transactionId", transactionId + 1);
    //   break;
    // case ble.action.MONITOR_CHARACTERISTIC:
    //   const id =
    //     action.deviceIdentifier +
    //     " " +
    //     action.serviceUUID +
    //     " " +
    //     action.characteristicUUID;
    //   if (!action.monitor) {
    //     _.set(state, ["operations", id, "state"], "cancel");
    //   }
    //   _.set(state, ["operations", id], {
    //     type: "monitor",
    //     state: "new",
    //     deviceIdentifier: action.deviceIdentifier,
    //     serviceUUID: action.serviceUUID,
    //     characteristicUUID: action.characteristicUUID,
    //     transactionId: id
    //   });
    //   break;
    // case ble.action.EXECUTE_TRANSACTION:
    //   _.set(state, ["operations", action.transactionId, "state"], "inProgress");
    //   break;
    // case ble.action.COMPLETE_TRANSACTION:
    //   _.set(
    //     state,
    //     "operations",
    //     _.omit(state.operations, action.transactionId)
    //   );
    //   break;
    // case ble.action.PUSH_ERROR:
    //   _.set("errors", _.get(state, "errors").push(action.errorMessage));
    //   break;
    // case ble.action.POP_ERROR:
    //   _.set("errors", _.get(state, "errors").pop());
    // default:
    //   break;
}  
return state;
};

export { bleReducers };
