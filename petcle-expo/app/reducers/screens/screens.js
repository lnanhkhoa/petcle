import _ from 'lodash'
import { act_navigate } from '../../actions/';

let defaults = {
  current: undefined,
  gotoScreen: undefined,
  paramsScreen: undefined
}

export const screensReducers = (screens = defaults, action) => {
  switch (action.type) {
    case act_navigate.action.NAVIGATE.CURRENT:
      return {
        ...screens,
        current: action.data.screen
      }
      case act_navigate.action.NAVIGATE.GOTO_SCREEN:
      return {
        ...screens,
        gotoScreen: action.data.screen,
        paramsScreen: action.data.params
      }
      case act_navigate.action.NAVIGATE.CLEAR_DATA_NAVIGATE:
        return {
          ...screens,
          gotoScreen: undefined
        }
      s
      default:
    return screens;
  }
}