import { act_process_event } from '../../actions/';

let resDefault = {
  qrcode_push: undefined,
  qrcode_login: undefined
}

export const processReducers = (res = resDefault, action) => {
  switch (action.type) {
    case act_process_event.action.DATA_QRCODE_PUSH.CREATE:
    return {
      ...res,
      qrcode_push: action.data
    }
    case act_process_event.action.DATA_QRCODE_PUSH.REMOVE:
    return {
      ...res,
      qrcode_push: undefined
    }
    case act_process_event.action.DATA_QRCODE_LOGIN.CREATE:
      return {
        ...res,
        qrcode_login: action.data
      }
    case act_process_event.action.DATA_QRCODE_LOGIN.REMOVE:
      return {
        ...res,
        qrcode_login: undefined
      }
    default:
      return res
  }
}