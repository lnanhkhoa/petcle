import * as act_ from '../actions/models';
import * as act_selectSize from '../actions/models/lockerTypes';

let sizeLocker = {
    id : 1,
};

const sizeLockerReducers = (times = 1, action) => {
    switch (action.type) {
        case act_selectSize.action.DECREMENT_LOCKER:
            return --times;
        case act_selectSize.action.INCREMENT_LOCKER:
            return ++times;
        default:
            return times;
    }
}
export { sizeLockerReducers };