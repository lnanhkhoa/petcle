import { act_unlock } from '../../actions/'

export const unlockProgressReducers = (progress = 0, action) => {
  if (progress >= 1){
    progress = progress - parseInt(progress)
  }
  switch (action.type) {
    case act_unlock.action.UNLOCK_PROGRESS_CHANGE.STRING:
    return (progress + 0.1);
    default:
    // do not thing
      return progress;
  }
}