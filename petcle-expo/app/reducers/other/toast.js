import {act_toast} from '../../actions/';

export const toastReducers = (toast = { message: 'this is toast', type: 'noti' }, action) => {
  switch (action.type) {
    case act_toast.action.SHOW_TOAST:
      return { ...toast, message: action.data.message, type: action.data.type };
      break;
    case act_toast.action.CLEAR_TOAST:
      return { ...toast, message: undefined, type: undefined };
      break;
    default:
      return toast
  }
}