import { act_elocker} from '../../actions/'

const progressReducers = (progress = 0, action) => {
    if (progress < 1 ){
        switch (action.type) {
            case act_elocker.action.GET_PAGE.FETCH_SUCCESS:
                progress += 0.5;
                break;
            default:
            // do not thing
        }
    }
    return progress;
}
export { progressReducers };