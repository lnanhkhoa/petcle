import * as act_ from '../actions/models';
import * as act_promotion from '../actions/models/promotion';

let promotion = {
    promotion_id: 1,
};

const promotionReducers = (promotion = [], action) => {
    switch (action.type) {
        case act_.action.FETCH_SUCCESS:
            //console.log(action.elockers.length);
            return action.res
        case act_.action.FETCH_FAIL:
            return action.res
        default:
            return {meta: undefined, data: []}
    }
}
export { promotionReducers };