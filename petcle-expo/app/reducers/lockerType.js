import * as act_lockerType from '../actions/models/lockerTypes';

let resLockertypes = {
    meta: {
        success: undefined,
    },
    data: [],
    error: undefined
}


const lockertypeReducers = (lockertypes = [], action) => {
    switch (action.type) {
        case act_lockerType.action.GET_INFO_LOCKERTYPE.FETCH_SUCCESS:
            resLockertypes = { ...action.res }
            break;
        case act_lockerType.action.GET_INFO_LOCKERTYPE.FETCH_FAIL:
            resLockertypes = { ...action.res }
            break;
        default:
            // do not thing
    }
    return resLockertypes
}
export { lockertypeReducers };  