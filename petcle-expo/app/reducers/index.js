import { combineReducers } from 'redux';

// import { progressReducers, unlockProgressReducers } from './other/';
// import { elockerReducers } from './elocker/';
// import { bookingReducers } from './booking/';
// import { meReducers } from './me/'
// import { screensReducers } from './screens/'
// import { hardwareReducers } from './hardware/';
// import { processReducers } from './processEvent/'
// import {toastReducers} from './other/'
// import { socketReducers } from './socket/'
// import { promotionReducers } from './promotion';
// import { sizeLockerReducers } from './selectSize';
// import { lockertypeReducers } from './lockerType';
// import { lockerListByUserReducers } from './lockListByUser';
// import { mapConfigReducers } from './home/map';
// import { updateBookingReducers } from './booking/updateBooking';
// import { bookingConfirmReducers } from './booking/bookingConfirm';
// import { getBookingReducers } from './booking/getBookingByUserId'
// import { putBookingReducers } from './booking/putBooking'
// import { exportBookingReducers } from './booking/exportBooking'
// import { dashboardReducers } from './dashboard/'

// import { createNavigationReducer} from 'react-navigation-redux-helpers';
// import {RootNavigator} from '../config/navigation/appNavigator'
// const navReducer = createNavigationReducer(RootNavigator);

const allReducers = combineReducers({
    // progressReducers,
    // nav: navReducer,
    // unlockProgressReducers,
    // processReducers,
    // socketReducers,
    // screensReducers,
    // elockerReducers,
    // bookingReducers,
    // meReducers,
    // hardwareReducers,
    // toastReducers
    // promotionReducers,
    // sizeLockerReducers,
    // lockertypeReducers,
    // mapConfigReducers,
    // lockerListByUserReducers,
    // addBookingReducers,
    // updateBookingReducers,
    // bookingConfirmReducers,
    // getBookingReducers,
    // putBookingReducers,
    // exportBookingReducers,
    // dashboardReducers

    //you can add more reducers here, separated by , !
});
export default allReducers; 