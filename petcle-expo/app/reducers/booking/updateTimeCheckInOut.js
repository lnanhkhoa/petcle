import {act_booking} from '../../actions/models/booking';

let resUpdateTime = {
    meta: {
        success: undefined,
    },
    data: [],
    error: undefined
}


const updateTimeReducers = (resUpdateTime = [], action) => {
    switch (action.type) {
        case act_booking.action.UPDATE_CHECKIN_OUT_TIME.FETCH_SUCCESS:
            resUpdateTime = { ...action.res }
            break;
        case act_booking.action.UPDATE_CHECKIN_OUT_TIME.FETCH_FAIL:
            resUpdateTime = { ...action.res }
            break;
        default:
            // do not thing
    }
    return resUpdateTime
}
export { updateTimeReducers };  