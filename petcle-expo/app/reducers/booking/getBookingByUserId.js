import { act_booking } from '../../actions';

export const getBookingByUserId = {
  meta: {
    success: undefined,
  },
  data: [],
  error: undefined
}

export const getBookingByUserIdCase = (action) => {
  switch (action.type) {
    case act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_SUCCESS:
    case act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_FAIL:
      return{ 
        ...action.res 
      }
      break;
    default:
      return action;
  }
}