import _ from 'lodash'
import { act_booking } from '../../actions';

export const addBooking = {
  meta: {
    success: undefined,
  },
  data: [],
  error: undefined
}

export const addBookingCase = (action) => {
  switch (action.type) {
    case act_booking.action.ADD_BOOKING.STRING:
    case act_booking.action.ADD_BOOKING.FETCH_SUCCESS:
    case act_booking.action.ADD_BOOKING.FETCH_FAIL:
      // return _.set(addBooking, ['meta', 'success'], !addBooking.meta.success)
      return _.set(addBooking, { ...action.res })
      break;
    default:
      return addBooking;
  }
}