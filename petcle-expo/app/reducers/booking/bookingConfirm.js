import _ from 'lodash'
import { act_booking } from '../../actions/';

export const bookingConfirm = {
    meta: {
        success: undefined,
    },
    data: [],
    error: undefined
}

export const bookingConfirmCase = (action) => {
    switch (action.type) {
        case act_booking.action.CONFIRM_BOOKING.FETCH_SUCCESS:
        case act_booking.action.CONFIRM_BOOKING.FETCH_FAIL:
            return _.set(bookingConfirm, { ...action.res })
        default:
            return bookingConfirm
    }
}