import {act_booking} from '../../actions/models/booking';

let resDefaults = {
  meta: {
    success: undefined,
  },
  data: [],
  error: undefined
}

const exportBookingReducers = (state = resDefaults, action) => {
  switch (action.type) {
    case act_booking.action.ADD_BOOKING.FETCH_SUCCESS:
      state = { ...action.res }
      break;
    case act_booking.action.ADD_BOOKING.FETCH_FAIL:
      state = { ...action.res }
      break;

    default:
    // do not thing
  }
  return state
}
export { exportBookingReducers };  