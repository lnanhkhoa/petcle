import _ from 'lodash'
import { act_booking } from '../../actions';
import { addBooking, addBookingCase } from './addBooking'
import { bookingConfirm, bookingConfirmCase } from './bookingConfirm'
import { getBookingByUserId, getBookingByUserIdCase } from './getBookingByUserId'
import { getPackagesByUserId, getPackagesByUserIdCase } from './getPackagesByUserId'


let defaults = {
    addBooking,
    bookingConfirm,
    getBookingByUserId,
    getPackagesByUserId
}

export const bookingReducers = (booking = defaults, action) => {

    switch (action.type) {
        case act_booking.action.ADD_BOOKING.FETCH_SUCCESS:
        case act_booking.action.ADD_BOOKING.FETCH_FAIL:
            return {
                ...booking,
                addBooking: addBookingCase(action)
            }
        case act_booking.action.CONFIRM_BOOKING.FETCH_SUCCESS:
        case act_booking.action.CONFIRM_BOOKING.FETCH_FAIL:
            return {
                ...booking,
                bookingConfirm: bookingConfirmCase(action)
            }
        case act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_SUCCESS:
        case act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_FAIL:
            return {
                ...booking,
                getBookingByUserId: getBookingByUserIdCase(action)
            }
        case act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_SUCCESS:
        case act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_FAIL:
            return {
                ...booking,
                getPackagesByUserId: getPackagesByUserIdCase(action)
            }
        default:
            return booking;
    }
}