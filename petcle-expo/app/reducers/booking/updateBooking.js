import {act_booking} from '../../actions/models/booking';

let resUpdateBooking = {
    meta: {
        success: undefined,
    },
    data: [],
    error: undefined
}

const updateBookingReducers = (resUpdateBooking = [], action) => {
    switch (action.type) {
        case act_booking.action.UPDATE_BOOKING.FETCH_SUCCESS:
            resUpdateBooking = { ...action.res }
        break;
        case act_booking.action.UPDATE_BOOKING.FETCH_FAIL:
            resUpdateBooking = { ...action.res }
        break;
        default:
            // do not thing
    }
    return resUpdateBooking
}
export { updateBookingReducers };  