import {act_booking} from '../../actions';

let resCreateBooking = {
    data: [],
    error: undefined
}

const createBookingReducers = (resCreateBooking = [], action) => {
    resCreateBooking = { ...action.res }
    return resCreateBooking
}
export { createBookingReducers };  