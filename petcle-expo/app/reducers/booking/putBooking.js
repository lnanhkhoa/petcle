import {act_booking} from '../../actions/models/booking';

let resDefaults = {
  meta: {
    success: undefined,
  },
  data: [],
  error: undefined
}


const putBookingReducers = (state = resDefaults, action) => {
  switch (action.type) {
    case act_booking.action.PUT_BOOKING.FETCH_SUCCESS:
      state = { ...action.res }
      break;
    case act_booking.action.PUT_BOOKING.FETCH_FAIL:
      state = { ...action.res }
      break;
    default:
    // do not thing
  }
  return state
}
export { putBookingReducers };  