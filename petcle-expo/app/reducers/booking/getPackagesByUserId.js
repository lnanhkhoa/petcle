import { act_booking } from '../../actions';

export const getPackagesByUserId = {
  meta: {
    success: undefined,
  },
  data: [],
  error: undefined
}

export const getPackagesByUserIdCase = (action) => {
  switch (action.type) {
    case act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_SUCCESS:
    case act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_FAIL:
      return{ 
        ...action.res 
      }
    default:
      return action;
  }
}