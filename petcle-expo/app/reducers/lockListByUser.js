import {act_booking} from '../actions/models/booking';

let resLockerList = {
    meta: {
        success: undefined,
    },
    data: [],
    error: undefined
}

const lockerListByUserReducers = (lockerlist = [], action) => {
    switch (action.type) {
        case act_booking.action.GET_LOCKER_BY_USER.FETCH_SUCCESS:
            resLockerList = { ...action.res }
            break;
        case act_booking.action.GET_LOCKER_BY_USER.FETCH_FAIL:
            resLockerList = { ...action.res }
            break;
        default:
            // do not thing
    }
    return resLockerList
}
export { lockerListByUserReducers };  