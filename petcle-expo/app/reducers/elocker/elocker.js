import { act_elocker } from '../../actions';
import _ from 'lodash'

let defaults = {
    getPage: {
        meta: undefined,
        data: [],
        error: undefined
    },
    elockerBookingPushAction: {
        meta: undefined,
        data: [],
        error: undefined
    },
    getById: {
        meta: undefined,
        data: [],
        error: undefined
    }
}

const elockerReducers = (elockers = defaults, action) => {
    switch (action.type) {
        case act_elocker.action.GET_PAGE.FETCH_SUCCESS:
        case act_elocker.action.GET_PAGE.FETCH_FAIL:
            return {
                ...elockers,
                getPage: action.res
            }
        case act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.FETCH_SUCCESS:
        case act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.FETCH_FAIL:
            return {
                ...elockers,
                elockerBookingPushAction: action.res
            }
        case act_elocker.action.GET_BY_ID.STRING:
            return {
                ...elockers,
                getById: action.res
            }
        default:
            return elockers;
    }
}
export { elockerReducers };