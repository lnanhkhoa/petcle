import {  } from '../../actions/';

let resDefault = {
  meta: undefined,
  data: [],
  error: undefined,
  login_loading: false
}

export const meReducers = (res = resDefault, action) => {
  switch (action.type) {
    case act_me.action.SOCIAL_LOGIN.FETCH_SUCCESS:
      res = { ...action.res, login_loading: false };
      break;
    case act_me.action.SOCIAL_LOGIN.FETCH_FAIL:
      res = { ...action.res, login_loading: false };
      break;
    default:
      if (action.login_loading == true) res.login_loading = action.login_loading;
    //else do not thing
  }
  return res
}