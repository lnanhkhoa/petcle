import * as act_map from '../../actions/screens/home/map'

let _mapConfig = {
    mapRegion: undefined,
    myPosition: undefined,
    gpsAccuracy: undefined,
    bounds: undefined,
};

const mapConfigReducers = (mapConfig = _mapConfig, action) => {
    switch (action.type) {
        case act_map.action.GET_MY_LOCATION.FETCH_SUCCESS:
            mapConfig = { ...action.mapConfig }
            break;
        case act_map.action.GET_MY_LOCATION.FETCH_FAIL:
            mapConfig = { ...action.mapConfig }
            break;
        case act_map.action.REGION_CHANGE:
            mapConfig = {
                ...mapConfig,
                mapRegion: action.region,
                bounds: {
                    northeast: {
                        latitude: action.region.latitude + action.region.latitudeDelta / 2,    // northLat - max lat
                        longitude: action.region.longitude + action.region.longitudeDelta / 2, // eastLng - max lng
                    },
                    southwest: {
                        latitude: action.region.latitude - action.region.latitudeDelta / 2,    // southLat - min lat
                        longitude: action.region.longitude - action.region.longitudeDelta / 2, // westLng - min lng
                    }
                }
            }
        default:
        // do not thing
    }
    return mapConfig;
}
export { mapConfigReducers };

