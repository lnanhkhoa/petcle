import { act_booking, act_socket, act_toast } from '../../actions/'
import { checkCorrectUserBooking } from '../../special/';
import { socketClient } from './socketClient';
import { store } from '../../store'

import { config } from '../../config/config.product'
const elocker_authorization = config.elocker.elocker_authorization;
class elockerSocket {
  constructor() {
    this.state = {
      token_key: elocker_authorization
    }
    this.SocketCenter = new socketClient(this.state.token_key);
  }

  async connectServer() {
    this.SocketCenter.initialListener()
    this.SocketCenter.registerListener('react redux action server', this.userBookingConfirm);
    store.dispatch(act_toast.creators.show('connect socket successfully!', 'success'))
  }

  userBookingConfirm = (event, data) => {
    switch (data.type) {
      case act_socket.action.BOOKING_ELOCKER_STATUS_CHANGE:
        store.dispatch(act_socket.creators.eventBookingElockerStatusChange(data));
        break;
      case act_socket.action.BOOKING_USER_STATUS_CHANGE:
        store.dispatch(act_socket.creators.eventBookingUserStatusChange(data));
        break;
      case act_socket.action.QRCODE_UNLOCK_ACTION:
        store.dispatch(act_socket.creators.qrcodeUnlockAction(data));
        break;
      case act_socket.action.QRCODE_UNLOCK_LOGIN:
        store.dispatch(act_socket.creators.qrcodeUnlockLogin(data));
        break;
      case act_socket.action.HARDWARE_WRITELINE:
        store.dispatch(act_socket.creators.eventHardwareWriteline(data));
        break;
      default:
        break;
    }
    // let checkUserBooking = new checkCorrectUserBooking(data);
    // if(checkUserBooking.resultsCheck()){
    //   store.dispatch(
    //     act_booking.creators.fetchBookingConfirm({
    //       elocker_authorization: this.state.token_key,
    //       booking_id: data.data.id
    //     })
    //   );
    // }
  }
}

// const ElockerSocket = new elockerSocket();
// ElockerSocket.connectServer();

// export {
//   ElockerSocket
// }