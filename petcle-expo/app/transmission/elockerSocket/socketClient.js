import SocketIOClient from 'socket.io-client';
import { server } from '../../config/server'
import {store } from '../../store'
import {act_toast} from '../../actions/'

export class socketClient {
    constructor(token_key) {
        this.idReconnect = null;
        this.status = 'disconnected',
        this.allowAccesswithTokenWhenReconnect = false;
        this.token_key = token_key;
        this.socket = new SocketIOClient(`ws://${server.host}:${server.socket.port}`);
        this.socket.connect();
    }

    initialListener() {
        this.socket.on('connect', () => {
            console.log(`Client has already connected!`);
            clearInterval(this.idReconnect)
            this.connectWithToken(null, (event, result) => {
                if (!!result.meta.success) {
                    this.status = 'connected'
                    console.log('connect socket');
                    store.dispatch(act_toast.creators.show('connect socket successfully!', 'success'))
                }
            });
        })
        this.socket.on('disconnect', () => {
            console.log(`Client disconnected!`)
            this.status = 'disconnected'
            store.dispatch(act_toast.creators.show('connect socket successfully!', 'success'))
            this.idReconnect = setInterval(() => {
                this.socket.connect();
            }, 2000);
        })
    }

    status() {
        return {
            connect: this.socket.connected,
            disconnect: this.socket.disconnected
        }
    }

    connectWithToken(tokenUser, next) {
        if (!!this.tokenUser) this.token_key = tokenUser;
        let event = 'connection/elocker_join';
        let data = {
            elocker_authorization: this.token_key
        };
        this.emitEvent(event, data, next);
    }

    registerListener(event, next) {
        this.socket.on(event, (data) => {
            if (!!next) next(event, data);
        })
    }

    removeListener(event, next) {
        this.socket.off(event, (data) => {
            console.log(`SocketIO REMOVE: ${event}`);
            if (!!next) next(event, data);
        });
    }

    emitEvent(event, data, next) {
        this.socket.emit(event, data, (result) => {
            console.log(`SEND [${event}]: ${JSON.stringify(result)}\n`);
            if (!!next) next(event, result);
        });
    }

    logEmitEvent(event, data) {
        console.log(`Socket [${event}]: ${JSON.stringify(data)}`);
    }
}