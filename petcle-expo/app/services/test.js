import end_point_name from '../services/elocker_api/endpoint_name';
import { call, all, fork } from 'redux-saga/effects';

// function* get_lockertype() {
//     console.log(end_point_name.lockertype.lockertype_info)
//     const response = yield fetch(end_point_name.promotion.lockertype_info, {
//         method: 'GET',
//         headers: {
//             'Accept': 'application/json',
//             'Accept-Encoding': 'gzip, deflate, br',
//         },
//         body: '',
//     });
//     const data = yield all([call([response, response.json])]);
    
//     return data;    
// }

// export const api = {
//     get_lockertype
// }

async function get_lockertype() {       
    try {   
        console.log(end_point_name.lockertype.lockertype_info);
        const api = end_point_name.lockertype.lockertype_info;
        let response = await fetch(api);
        let responseJson = await response.json();
        return responseJson.data;
    } catch (error) {
        console.log(error); 
    }
}

export {get_lockertype}