import end_point_name from '../endpoint_name';
import { call, all, fork } from 'redux-saga/effects';

function* push_action(paging) {
    console.log('paging', paging)
    const response = yield fetch(end_point_name.elocker.elocker_booking_push_action, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'elocker_authorization': paging.elocker_authorization,
            'user_authorization': paging.user_authorization,
            'booking_id': paging.booking_id,
            'action_type': paging.action_type
        },
        body: JSON.stringify({
            'list_locker': [
                ...paging.list_locker
            ]
        }),
    });
    const data = yield all([call([response, response.json])]);
    console.log(data[0]);
    return data[0];
}

export const api = {
    push_action
}