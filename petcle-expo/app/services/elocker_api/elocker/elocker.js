//import * as act_ from '../../../actions/models';
import { act_elocker } from '../../../actions/';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { api } from './api';


// function* fetchElockerGetPage(action) {
//     try {
//         //console.log('fetchElockerGetPage')
//         //console.log(action)
//         const res = yield api.get_page(action.paging);
//         if (res.meta.success) {
//             yield put({
//                 type: act_elocker.action.GET_PAGE.FETCH_SUCCESS,
//                 res: res
//             });
//         }
//         else {
//             yield put({
//                 type: act_elocker.action.GET_PAGE.FETCH_FAIL,
//                 res: res
//             });
//         }
//     } catch (error) {
//         yield put({ type: act_elocker.action.GET_PAGE.FETCH_FAIL, res: { error } });
//     }
// }

function* fetchElokerBookingPushAction(action) {
  try {
    const res = yield api.push_action(action.data);
    if (res.meta.success) {
      yield put({
        type: act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.FETCH_FAIL, res: { error } });
  }
}

// watch Home
export function* watchFetchElocker() {
  yield all([
    // takeLatest(act_elocker.action.GET_PAGE.STRING, fetchElockerGetPage),
    // takeLatest(act_elocker.action.SEARCH.STRING, fetchElockerGetPage),
    // takeLatest(act_elocker.action.SORT.STRING, fetchElockerGetPage),
    // takeLatest(act_elocker.action.FILTER.STRING, fetchElockerGetPage),
    takeLatest(act_elocker.action.ELOCKER_BOOKING_PUSH_ACTION.STRING, fetchElokerBookingPushAction),
  ]);
}