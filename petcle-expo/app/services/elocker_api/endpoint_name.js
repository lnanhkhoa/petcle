
import { server } from '../../config/server'

const url_base = `http://${server.host}:${server.api.port}/api/`;

const end_point_name = {
    // User 
    'user': {
        user_info: `${url_base}user/me`, // thông tin người dùng
        user_login: `${url_base}user/login`, // người dùng đăng nhập
        user_login_with_facebook: `${url_base}user/login_with_facebook`, // người dùng đăng nhập facebook
        user_login_with_google: `${url_base}user/login_with_google`, // người dùng đăng nhập google
        user_register: `${url_base}user/user_register`, // người dùng đăng ký
    },
    // Elocker
    'elocker': {
        elocker_info: `${url_base}elocker`, // lấy thông tin của 1 trạm elocker
        elocker_get_page: `${url_base}elocker/get_page`, // danh sach elocker phân trang
        elocker_add: `${url_base}elocker/add`, // thêm 1 trạm elocker vào hệ thống
        elocker_register: `${url_base}elocker/token/initialize`, // đăng ký 1 elocker
        elocker_token_renew: `${url_base}elocker/token/renew`, // xin lại token đã hết hạn
        elocker_update_status: `${url_base}elocker/lockers/update_status`, // update trạng thái locker
        elocker_booking_push_action: `${url_base}elocker/booking/push_action`
    },
    // Elocker Token
    'elocker_token': {
        elocker_token_approve: `${url_base}Elocker_Token/post_elocker_token_apprule`, // duyệt 1 trạm elocker trong hệ thống
        elocker_token_initialize: `${url_base}Elocker_Token/post_elocker_token_initialize`, //đăng ký 1 trạm elocker vào hệ thống
        elocker_token_renew: `${url_base}Elocker_Token/post_elocker_token_renew`, // xin lại token đã hết hạn
    },
    // Locker
    'locker': {
        locker_info: `${url_base}Locker/get_locker`, // thông tin các locker của elocker
        locker_add: `${url_base}Locker/post_locker_add`, // thêm 1 locker vào hệ thống
        locker_update: `${url_base}Locker/post_locker_update`, // cập nhật 1 locker vào hệ thống
    },
    // Package
    'package': {
        package_add: `${url_base}Package/post_package_me_add`, // thêm 1 mặt hàng vào hệ thống
        package_delete: `${url_base}Package/post_package_me_delete`, // xóa 1 mặt hàng vào hệ thống
        package_update: `${url_base}Package/post_package_me_update`, // cập nhật 1 mặt hàng vào hệ thống
    },
    // ConsignmentBill
    'consignmentbill': {
        consignmentbill_info: `${url_base}Consignmentbill/get_consignmentbill`, // lấy thông tin của 1 consignmentbill
        consignmentbill_add: `${url_base}Consignmentbill/post_consignmentbill_add`, // thêm 1 consignmentbill vào hệ thống
        consignmentbill_delete: `${url_base}Consignmentbill/post_consignmentbill_delete`, // xóa 1 consignmentbill vào hệ thống
        consignmentbill_update: `${url_base}Consignmentbill/post_consignmentbill_update`, // cập nhật 1 consignmentbill vào hệ thống
    },
    // Hascode
    'hascode': {
        hascode_info: `${url_base}Hascode/get_hascode`, // lấy thông tin của 1 hascode
        hascode_add: `${url_base}Hascode/post_hascode_add`, // thêm 1 hascode vào hệ thống
        hascode_delete: `${url_base}Hascode/post_hascode_delete`, // xóa 1 hascode vào hệ thống
        hascode_update: `${url_base}Hascode/post_hascode_update`, // cập nhật 1 hascode vào hệ thống
    },
    // Hub
    'hub': {
        hub_info: `${url_base}Hub/get_hub`, // lấy thông tin của 1 hub
        hub_add: `${url_base}Hub/post_hub_add`, // thêm 1 hub vào hệ thống
        hub_delete: `${url_base}Hub/post_hub_delete`, // xóa 1 hub vào hệ thống
        hub_update: `${url_base}Hub/post_hub_update`, // cập nhật 1 hub vào hệ thống
    },
    // Image
    'image': {
        image_info: `${url_base}Image/get_image`, // lấy thông tin của 1 image
        image_add: `${url_base}Image/post_image_add`, // thêm 1 image vào hệ thống
        image_delete: `${url_base}Image/post_image_delete`, // xóa 1 image vào hệ thống
        image_update: `${url_base}Image/post_image_update`, // cập nhật 1 image vào hệ thống
        image_upload: `${url_base}Image/post_image_update`, // upload image trong hệ thống
    },
    // Location
    'location': {
        location_info: `${url_base}Location/get_location`, // lấy thông tin của 1 location
        location_add: `${url_base}Location/post_location_add`, // thêm 1 location vào hệ thống
        location_delete: `${url_base}Location/post_location_delete`, // xóa 1 location trong hệ thống
        location_update: `${url_base}Location/post_location_update`, // cập nhật 1 location vào hệ thống
    },
    // Payment
    'payment': {
        payment_info: `${url_base}Payment/get_payment`, // lấy thông tin của 1 payment
        payment_add: `${url_base}Payment/post_payment_add`, // thêm 1 payment vào hệ thống
        payment_delete: `${url_base}Payment/post_payment_delete`, // xóa 1 payment trong hệ thống
        payment_update: `${url_base}Payment/post_payment_update`, // cập nhật 1 payment vào hệ thống
    },
    // Lockerbooking
    'lockerbooking': {
        lockerbooking_info: `${url_base}Lockerbooking/get_lockerbooking`, // lấy thông tin của 1 lockerbooking
        lockerbooking_add: `${url_base}Lockerbooking/post_lockerbooking_add`, // thêm 1 lockerbooking vào hệ thống
        lockerbooking_delete: `${url_base}Lockerbooking/post_lockerbooking_delete`, // xóa 1 lockerbooking trong hệ thống
        lockerbooking_update: `${url_base}Lockerbooking/post_lockerbooking_update`, // cập nhật 1 lockerbooking vào hệ thống
    },
    // Paymentinfo
    'paymentinfo': {
        paymentinfo_info: `${url_base}Paymentinfo/get_paymentinfo`, // lấy thông tin của 1 paymentinfo
        paymentinfo_add: `${url_base}Paymentinfo/post_paymentinfo_add`, // thêm 1 paymentinfo vào hệ thống
        paymentinfo_delete: `${url_base}Paymentinfo/post_paymentinfo_delete`, // xóa 1 paymentinfo trong hệ thống
        paymentinfo_update: `${url_base}Paymentinfo/post_paymentinfo_update`, // cập nhật 1 paymentinfo vào hệ thống
    },
    // Promotion
    'promotion': {
        promotion_info: `${url_base}Promotion/get_promotion`, // lấy thông tin của 1 promotion
        promotion_add: `${url_base}Promotion/post_promotion_add`, // thêm 1 promotion vào hệ thống
        promotion_delete: `${url_base}Promotion/post_promotion_delete`, // xóa 1 promotion trong hệ thống
        promotion_update: `${url_base}Promotion/post_promotion_update`, // cập nhật 1 promotion vào hệ thống
    },
    //LockerType
    'lockertype': {
        lockertype_info: `${url_base}locker_type/getlockertype`, // lấy thông tin của 1 promotion
    },
    'booking': {
        add_booking: `${url_base}booking/add`,
        get_by_userID: `${url_base}elocker/booking/get_by_user_id`,
        update_booking: `${url_base}booking/update`,
        put_booking: `${url_base}elocker/booking/put`,
        done_booking: `${url_base}elocker/booking/done`,
        booking_confirm: `${url_base}elocker/booking/confirm`
    },
};

export default end_point_name;