//import * as act_ from '../../../actions/models';
import * as act_lockerType from '../../../actions/models/lockerTypes';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { api } from './api';

function* fetchLockerType(action) {
    try {
        const res = yield api.get_lockertype();
        if (res.meta.success) {
            yield put({
                type: act_lockerType.action.GET_INFO_LOCKERTYPE.FETCH_SUCCESS,
                res: res
            });
        }
        else {
            yield put({
                type: act_lockerType.action.GET_INFO_LOCKERTYPE.FETCH_FAIL,
                res: res
            });
        }
    } catch (error) {
        yield put({ type: act_lockerType.action.GET_INFO_LOCKERTYPE.FETCH_FAIL, res: { error } });
    }
}

export function* watchFetchLockerType() {
    yield all([
        takeLatest(act_lockerType.action.GET_INFO_LOCKERTYPE.STRING, fetchLockerType),
    ]); 
}