import end_point_name from '../endpoint_name';
import { call, all, fork } from 'redux-saga/effects';

function* get_lockertype() {

    const response = yield fetch(end_point_name.lockertype.lockertype_info, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}

export const api = {
    get_lockertype
}