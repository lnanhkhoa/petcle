//import * as act_ from '../../../actions/models';
import { act_booking } from '../../../actions/models/booking.js';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { api } from './api';


function* fetchBookingConfirm(action) {
  try {
    const res = yield api.booking_confirm(action.data);
    if (res.meta.success) {
      yield put({
        type: act_booking.action.CONFIRM_BOOKING.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_booking.action.CONFIRM_BOOKING.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_booking.action.CONFIRM_BOOKING.FETCH_FAIL, res: { error } });
  }
}

function* fetchBookingByUserId(action) {
  try {
    const res = yield api.getBooking_byUserId(action.data);
    if (res.meta.success) {
      yield put({
        type: act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_booking.action.GET_BOOKING_BY_USER_ID.FETCH_FAIL, res: { error } });
  }
}

function* fetchPackageByUserId(action) {
  try {
    const res = yield api.getPackage_byUserId(action.data);
    if (res.meta.success) {
      yield put({
        type: act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_booking.action.GET_PACKAGE_BY_USER_ID.FETCH_FAIL, res: { error } });
  }
}


function* fetchPutBooking(action) {
  try {
    const res = yield api.put_booking(action.data);
    if (res.meta.success) {
      yield put({
        type: act_booking.action.PUT_BOOKING.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_booking.action.PUT_BOOKING.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_booking.action.PUT_BOOKING.FETCH_FAIL, res: { error } });
  }
}


function* fetchExportElockerBooking(action) {
  try {
    const res = yield api.done_booking(action.data);
    if (res.meta.success) {
      yield put({
        type: act_booking.action.EXPORT_ELOCKER_BOOKING.FETCH_SUCCESS,
        res: res
      });
    }
    else {
      yield put({
        type: act_booking.action.EXPORT_ELOCKER_BOOKING.FETCH_FAIL,
        res: res
      });
    }
  } catch (error) {
    yield put({ type: act_booking.action.EXPORT_ELOCKER_BOOKING.FETCH_FAIL, res: { error } });
  }
}

// watch Home
export function* watchFetchBooking() {
  yield all([
    takeLatest(act_booking.action.CONFIRM_BOOKING.STRING, fetchBookingConfirm),
    takeLatest(act_booking.action.GET_BOOKING_BY_USER_ID.STRING, fetchBookingByUserId),
    takeLatest(act_booking.action.GET_PACKAGE_BY_USER_ID.STRING, fetchPackageByUserId),
    // takeLatest(act_booking.action.PUT_BOOKING.STRING, fetchPutBooking),
    // takeLatest(act_booking.action.EXPORT_ELOCKER_BOOKING.STRING, fetchExportElockerBooking),
  ]);
}