import end_point_name from '../endpoint_name';
import { call, all, fork } from 'redux-saga/effects';

function* booking_confirm(paging) {
    const response = yield fetch(end_point_name.booking.booking_confirm, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            elocker_authorization :paging.elocker_authorization,
            booking_id : paging.booking_id
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}

function* getBooking_byUserId(paging) {
    let query = `?booking_status_id=5&sort=booking_status_id&set=desc&page=1&page_size=10`;
    const response = yield fetch(end_point_name.booking.get_by_userID + query, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            user_authorization: paging.user_authorization,
            elocker_authorization: paging.elocker_authorization
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}

function* getPackage_byUserId(paging) {
    let query = `?booking_status_id=8&sort=booking_status_id&set=desc&page=1&page_size=10`;
    const response = yield fetch(end_point_name.booking.get_by_userID + query, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
            user_authorization: paging.user_authorization,
            elocker_authorization: paging.elocker_authorization
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    console.log(data[0])
    return data[0];
}

function* put_booking(paging) {
    const response = yield fetch(end_point_name.booking.put_booking, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            elocker_authorization: paging.elocker_authorization,
            booking_id: paging.booking_id
        },
        body: JSON.stringify({
            list_locker: paging.list_locker
        }),
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}

function* done_booking(paging) {
    const response = yield fetch(end_point_name.booking.done_booking, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            elocker_authorization: paging.elocker_authorization,
            booking_id: paging.booking_id
        },
        body: ''
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}



export const api = {
    booking_confirm,
    getBooking_byUserId,
    put_booking,
    done_booking,
    getPackage_byUserId
}