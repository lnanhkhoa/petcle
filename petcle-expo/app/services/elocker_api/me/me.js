
import { act_me } from '../../../actions/';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { api } from './api';


function* fetchSocialLogin(action) {
  try {
    const res = undefined, infoRes = undefined;
    switch (action.social) {
      case 'facebook':
        res = yield api.login_with_facebook();
        break;
      case 'google':
        res = yield api.login_with_google();
        break;
      case 'server':
        res = yield api.login_user_pass(action);
      default:
        break;
    }
    if (res.meta.success) {
      infoRes = yield api.get_my_profile(res.data.token_key);
      if (infoRes.meta.success) {
        yield put({
          type: act_me.action.SOCIAL_LOGIN.FETCH_SUCCESS,
          res: { ...infoRes, data: { ...infoRes.data, last_login_date: res.data.last_login_date, token_key: res.data.token_key } }
        });
      }
    }
    else {
      yield put({
        type: act_me.action.SOCIAL_LOGIN.FETCH_FAIL,
        res: res || infoRes
      });
    }
  } catch (error) {
    yield put({ type: act_me.action.SOCIAL_LOGIN.FETCH_FAIL, res: { error } });
  }
}
// watch Home
export function* watchFetchLogin() {
  yield all([
    takeLatest(act_me.action.SOCIAL_LOGIN.STRING, fetchSocialLogin),
  ]);
}