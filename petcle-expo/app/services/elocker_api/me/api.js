import end_point_name from '../endpoint_name';
import { call, all, fork } from 'redux-saga/effects';
import _ from 'lodash'
import { Google, Facebook } from 'expo';

function* get_my_profile(token) {
    const response = yield fetch(end_point_name.user.user_info, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'dataType': 'json',
            'user_authorization': token
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}

function* login_with_facebook() {
    const { type, token, expires } = yield Facebook.logInWithReadPermissionsAsync('2054323254808553', {
        permissions: ['public_profile', 'email', 'user_friends'],
    });
    if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        const response = yield fetch(end_point_name.user.user_login_with_facebook, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ token_id: token }),
        });
        const data = yield all([call([response, response.json])]);
        return data[0];
        
    }
}

function* login_with_google() {
     // https://www.googleapis.com/auth/contacts	Manage your contacts
    // https://www.googleapis.com/auth/contacts.readonly	View your contacts
    // https://www.googleapis.com/auth/user.addresses.read	View your street addresses
    // https://www.googleapis.com/auth/user.birthday.read	View your complete date of birth
    // https://www.googleapis.com/auth/user.emails.read	View your email addresses
    // https://www.googleapis.com/auth/user.phonenumbers.read	View your phone numbers
    // https://www.googleapis.com/auth/userinfo.email	View your email address
    // https://www.googleapis.com/auth/userinfo.profile	View your basic profile info
    let iosClientId = '607876304371-t97marirdf0mtua1krgmu7oqs41vn0ti.apps.googleusercontent.com'
    // androidClientId: "805331432756-k4mfv9qddvdlb5dueb2j3deq7hmgfcqm.apps.googleusercontent.com",
    const { type, idToken } = yield Google.logInAsync({
        //androidClientId, androidClientId,
        iosClientId: iosClientId,
        scopes: [
            'profile', 'email',
            'https://www.googleapis.com/auth/user.phonenumbers.read',
            'https://www.googleapis.com/auth/user.birthday.read',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/user.addresses.read'
        ]
    });
    if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        
        const response = yield fetch(end_point_name.user.user_login_with_google, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ token_id: idToken, client_id : iosClientId }),
        });
        const data = yield all([call([response, response.json])]);
        return data[0];
    }
}

function* login_user_pass(paging) {
    let user_name = _.get(paging, ['data', 'user_name']);
    let password = _.get(paging, ['data', 'password']);
    const response = yield fetch(end_point_name.user.user_login, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ 
            user_name: user_name,
            password: password
         }),
    });
    const data = yield all([call([response, response.json])]);
    return data[0];
}


export const api = {
    login_with_facebook,
    get_my_profile,
    login_with_google,
    login_user_pass
}