import * as act_ from '../../../actions/models';
import * as act_promotion from '../../../actions/models/promotion';
//Saga effects
import { put, takeLatest,all } from 'redux-saga/effects';
import { api } from './api';

function* fetchPromotionGetInfo(action) {
    try {
        const res = yield api.get_promotion(action.promotion);
        if (res.meta.success) {
            yield put({
                type: act_.action.FETCH_SUCCESS,
                res: res
            });
        }
        else {
            yield put({
                type: act_.action.FETCH_FAIL,
                res: res
            });
        }
    } catch (error) {
        yield put({ type: act_.action.FETCH_FAIL, res: { error } });
    }
}
// watch Home
export function* watchFetchPromotionGetPage() {
    yield all([takeLatest(act_promotion.action.PROMOTION_GET_INFO, fetchPromotionGetInfo)]);
}