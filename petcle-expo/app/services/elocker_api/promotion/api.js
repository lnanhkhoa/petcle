import { endpoint } from '../_endpoint';
import { call, all, fork } from 'redux-saga/effects';

function* get_promotion(promotion) {

    let query = `?promotion_id=${promotion.promotion_id}`;

    const response = yield fetch(endpoint.promotion.GET_PROMOTION + query, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Accept-Encoding': 'gzip, deflate, br',
        },
        body: '',
    });
    const data = yield all([call([response, response.json])]);
    return data;
}

export const api = {
    get_promotion
}
