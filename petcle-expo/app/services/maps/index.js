//import * as act_ from '../../../actions/models';
import * as act_map from '../../actions/screens/home/map';
import {act_elocker} from '../../actions/models/elocker';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { api } from './api';


function* fetchGetMyLocation(action) {
    try {
        const myPosition = yield api.get_my_location();
        if (myPosition) {
            /**
            |--------------------------------------------------
            | dispatch action lấy toạ độ hoàn thành
            |--------------------------------------------------
            */
            const mapConfig = {
                myPosition: myPosition,
                mapRegion: {
                    latitude: myPosition.coords.latitude,
                    longitude: myPosition.coords.longitude,
                    latitudeDelta: 0.00922 * 1.5,
                    longitudeDelta: 0.00421 * 1.5,
                },
                gpsAccuracy: myPosition.coords.accuracy || map.gpsAccuracy,
                bounds: {
                    northeast: {
                        latitude: myPosition.coords.latitude + 0.00922 * 1.5 / 2,    // northLat - max lat
                        longitude: myPosition.coords.longitude + 0.00421 * 1.5 / 2, // eastLng - max lng
                    },
                    southwest: {
                        latitude: myPosition.coords.latitude - 0.00922 * 1.5 / 2,    // southLat - min lat
                        longitude: myPosition.coords.longitude - 0.00421 * 1.5 / 2, // westLng - min lng
                    }
                }
            }
            yield put({
                type: act_map.action.GET_MY_LOCATION.FETCH_SUCCESS,
                mapConfig
            });
            /**
            |--------------------------------------------------
            | dispatch action lấy các elocker gần toạ độ mới lấy
            |--------------------------------------------------
            */
            if (action.isFetchNearByMe == true) {
                yield put({
                    type: act_elocker.action.GET_PAGE.STRING,
                    paging: {
                        page : 1,
                        page_size:10,
                        nbm_enable: true,
                        nbm_current_lat: mapConfig.myPosition.coords.latitude,
                        nbm_current_long: mapConfig.myPosition.coords.longitude,
                        nbm_bounds_northeast_lat: mapConfig.bounds.northeast.latitude,
                        nbm_bounds_northeast_long: mapConfig.bounds.northeast.longitude,
                        nbm_bounds_southwest_lat: mapConfig.bounds.southwest.latitude,
                        nbm_bounds_southwest_long: mapConfig.bounds.southwest.longitude,
                    }
                });
            }

        } else {
            yield put({
                type: act_map.action.GET_MY_LOCATION.FETCH_FAIL,
                mapConfig: null
            });
        }
    } catch (error) {
        yield put({
            type: act_map.action.GET_MY_LOCATION.FETCH_FAIL,
            mapConfig: null,
            error
        });
    }
}
// watch Home
export function* watchMaps() {
    yield all([
        takeLatest(act_map.action.GET_MY_LOCATION.STRING, fetchGetMyLocation),
    ]);
}