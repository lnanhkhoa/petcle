import { call, all, fork } from 'redux-saga/effects';
import { Location, Permissions } from 'expo';
import _ from 'lodash';

function* get_my_location() {
    let { status } = yield Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
        Alert.alert('Permission to access location was denied');
    } else {
        return yield Location.getCurrentPositionAsync({ enableHighAccuracy: true, maximumAge: 30000 });
    }
}

export const api = {
    get_my_location
}