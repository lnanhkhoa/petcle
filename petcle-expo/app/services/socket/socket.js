import { act_socket } from '../../actions/';
//Saga effects
import { put, takeLatest, all } from 'redux-saga/effects';
import { socketFunction } from './functionSocket';

// watch Home
export function* watchSocket() {
  yield all([
    takeLatest(act_socket.action.BOOKING_USER_STATUS_CHANGE, socketFunction.bookingUserStatusChange),
    takeLatest(act_socket.action.BOOKING_ELOCKER_STATUS_CHANGE, socketFunction.bookingElockerStatusChange),
    takeLatest(act_socket.action.QRCODE_UNLOCK_ACTION, socketFunction.qrcodeUnlockAction),
    takeLatest(act_socket.action.QRCODE_UNLOCK_LOGIN, socketFunction.qrcodeUnlockLogin),
    takeLatest(act_socket.action.HARDWARE_WRITELINE, socketFunction.hardwareWrite)
  ]);
}