import _ from 'lodash'
import { store } from '../../store'
import NavigationService from '../../helper/navigationService';
import { NavigationActions} from 'react-navigation'
import { put } from 'redux-saga/effects';
import { routesName } from '../../config/navigation/routes'
import { dispatchResponseRedux } from '../../transmission/hardware/'
import processReadChannel from '../../transmission/hardware/ble/processReadChannel';
import { act_navigate, act_hardware, act_process_event, act_booking } from '../../actions/'
import { unlockAction } from '../../special/'
import { screensReducers } from '../../reducers/screens';
const elocker_authorization = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbG9ja2VyIjp7ImlkIjo2LCJkYXRhIjoie1wiZWxvY2tlcl9kYXRhXCI6IFwiZWxvY2tlclwifSJ9LCJpYXQiOjE1MzM4NzM1NTksImV4cCI6NDY1NTkzNzU1OX0.VZj89bmSGBHUjPJnSIGTdxLgojN9vWLyDxQCJjQsrDQ';

function* bookingUserStatusChange(paging) {
  console.log('receive event user')
  const data = paging.data;
  if (!!data.meta.success) { console.log('success') }
  let booking_status_id = _.get(data, ['payload', 'data', 'booking_status_id'])
  let booking_id = _.get(data, ['payload', 'booking_id'])
  switch (booking_status_id) {
    case 3:
      yield put(act_booking.creators.fetchBookingConfirm({
        booking_id: booking_id,
        elocker_authorization: elocker_authorization
      }));
      break;
    case 5:
      console.log('save data')
    default:
      break;
  }
  return true;
}

function* bookingElockerStatusChange(paging) {
  console.log('receive event elocker')
  return true;
}

function* qrcodeUnlockAction(paging) {
  console.log('receive event qrcode action')
  const data = paging.data;
  if (!!data.meta.success) {
    let action_type = _.get(data, ['payload', 'action_type'])
    let booking_id = _.get(data, ['payload', 'booking_id'])
    let user_authorization = _.get(data, ['payload', 'user_authorization'])
    let locker_index_start = _.get(data, ['payload', 'data', 'locker_booking', 'locker_index_start'])
    let locker_index_end = _.get(data, ['payload', 'data', 'locker_booking', 'locker_index_end'])
    let list_locker = _.range(locker_index_start, locker_index_end + 1, 1);

    switch (action_type) {
      case 'put':
        // NavigationService.dispatchReset(routesName.waiting.Loading)
        yield put(act_process_event.creators.createDataQrCodePush({
          action_type: 'put',
          booking_id: booking_id,
          user_authorization: user_authorization,
          list_locker: list_locker,
          elocker_authorization: elocker_authorization
        }))
        yield put(act_navigate.creators.gotoScreen({screen: routesName.waiting.Loading}))
        yield put(act_hardware.creators.command.moveTrayToDoor())
        break;
      case 'get':
        yield put(act_process_event.creators.createDataQrCodePush({
          action_type: 'get',
          booking_id: booking_id,
          user_authorization: user_authorization,
          list_locker: list_locker,
          elocker_authorization: elocker_authorization
        }))
        yield put(act_navigate.creators.gotoScreen({ screen: routesName.waiting.Loading }))
        yield put(act_hardware.creators.command.getPackageFromLocation({location:[0, 0]}))
        break;
      default:
        break;
    }


    // if (currentScreen === routesName.center.LoginQRCenter) {
    // yield put(act_navigate.creators.gotoScreen({
    //   screen: routesName.waiting.Loading
    // }))
    // }
  }
  return true;
}

function* qrcodeUnlockLogin(paging) {
  console.log('receive event qrcode login')
  const data = paging.data;
  if (!!data.meta.success) {
    yield put(act_process_event.creators.createDataQrCodeLogin({
      ...data.payload.data
    }))
  }
  yield put(act_navigate.creators.gotoScreen({ screen: routesName.center.HomeCenter }))
}

function* hardwareWrite(paging) {
  console.log('receive event hardware writeline')
  let string = paging.data.payload.writeline;
  let switchkey = string.slice(0, 5)
  switch (switchkey) {
    case '-->01':
      yield put(act_hardware.creators.command.moveTrayToDoor())
      break;
      case '-->02':
      yield put(act_hardware.creators.command.moveTrayToStore())
      break;
      case '-->03':
      yield put(act_hardware.creators.command.measurePackage())
      break;
    case '-->04':
      // yield put(act_hardware.creators.command.setPackageToLocation())
      break;
    case '-->05':
      // yield put(act_hardware.creators.command.measurePackage())
      break;

    default:
      yield put(act_hardware.creators.raw.read(string))
      dispatchResponseRedux(processReadChannel(string))
      break;
  }
  return true;
}

export const socketFunction = {
  bookingUserStatusChange,
  bookingElockerStatusChange,
  qrcodeUnlockAction,
  qrcodeUnlockLogin,
  hardwareWrite
}