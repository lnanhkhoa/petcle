import React from 'react';
import { View } from 'react-native';
import hoistNonReactStatic from 'hoist-non-react-statics';
import { StyleProvider } from '@shoutem/theme';
import getThemeStyle from './theme';

export const theme = getThemeStyle();

export function withTheme(Wrapped) {
    class ThemeProvider extends React.Component {
        constructor(props) {
            super(props);
        }

        componentDidMount() {
        }

        componentWillUnmount() {
        }

        render() {
            return (
                <StyleProvider style={theme}>
                    <Wrapped {...this.props} />
                </StyleProvider>)
        }
    }

    hoistNonReactStatic(ThemeProvider, Wrapped);
    return ThemeProvider;
}
