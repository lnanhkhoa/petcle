import { Platform } from 'react-native';
import { isIphoneX } from 'react-native-iphone-x-helper'

export class UIConstants {
  static AppbarHeight = Platform.OS === 'ios' ? 44 : 56;
  static StatusbarHeight = Platform.OS === 'ios' ? isIphoneX() ? 35 : 20 : 10;
  static HeaderHeight = (Platform.OS === 'ios' ? 44 : 56) + (Platform.OS === 'ios' ? isIphoneX() ? 35 : 20 : 10);
}
