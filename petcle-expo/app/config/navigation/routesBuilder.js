import React from 'react';
import _ from 'lodash';
import { createStackNavigator } from 'react-navigation'
import { NavBar } from '../../screens/_global/navBar';
import transition from './transitions';
import { withTheme } from '../themeProvider';
import { StyleProvider } from '@shoutem/theme';
import {
  MainRoutes
} from './routes';

let main = {};
let flatRoutes = {};
(MainRoutes).map(function (route, index) {

  let wrapToRoute = (route) => {
    return {
      screen: withTheme(route.screen),
      title: route.title
    }
  };

  flatRoutes[route.id] = wrapToRoute(route);
  main[route.id] = wrapToRoute(route);
  for (let child of route.children) {
    flatRoutes[child.id] = wrapToRoute(child);
  }
});

let NavigationBar = withTheme(NavBar);

const appRoutes = Object.keys(main).reduce((routes, name) => {
  let stack_name = name;

  routes[stack_name] = {
    name: stack_name,
    screen: createStackNavigator(flatRoutes,{
      initialRouteName: name,
      headerMode: 'screen',
      cardStyle: { backgroundColor: 'transparent' },
      transitionConfig: transition,
      navigationOptions: ({ navigation, screenProps }) => ({
        gesturesEnabled: false,
        header: (headerProps) => {
          return <NavigationBar navigation={navigation} headerProps={headerProps} />
        }
      })
    })
  };

  return routes;
}, {});

export const AppRoutes = appRoutes;
export const DevRoutes = _.find(MainRoutes, (i) => { return i.isDev == true });
