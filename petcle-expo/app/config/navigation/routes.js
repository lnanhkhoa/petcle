import React from 'react';
import { SimpleLineIcons, Ionicons } from '../../screens/_global/Icons';
import { Screens }from '../../screens';
import { theme } from '../../config/themeProvider';
import _ from 'lodash';
import { StackActions, NavigationActions } from 'react-navigation';

export const routesName = {
  home: {
    SplashScreen: 'SplashScreen',
    HomeComponent: 'HomeComponent',
    LoginScreen: 'LoginScreen',
    ForgotScreen: 'ForgotScreen',
    SettingsScreen: 'SettingsScreen',
    VideoScreen: 'VideoScreen',
    Testing: 'Testing',

  }
}
export const naviType = {
  stack: 'stack',
  draw: 'draw',
  tab: 'tab'
}
export const navigateReset = (route, params)=>{
  return StackActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: route,
        params: params  
      })
    ]
  });  
}

export const MainRoutes = [
  {
    id: routesName.home.VideoScreen,
    title: 'VideoScreen',
    icon: (style) => {
      return (<SimpleLineIcons styleName='black' name='home' />);
    },
    screen: Screens.VideoScreen,
    naviTypes: [naviType.stack],
    children: []
  },
  {
    id: routesName.home.ForgotScreen,
    title: 'ForgotScreen',
    icon: (style) => {
      return (<SimpleLineIcons styleName='black' name='home' />);
    },
    screen: Screens.ForgotScreen,
    naviTypes: [naviType.stack],
    children: []
  },
  {
    id: routesName.home.SplashScreen,
    title: 'SplashScreen',
    icon: (style) => {
      return (<SimpleLineIcons styleName='black' name='home' />);
    },
    screen: Screens.SplashScreen,
    naviTypes: [naviType.stack],
    children: []
  },
  {
    id: routesName.home.LoginScreen,
    title: 'LoginScreen',
    icon: (style) => {
      return (<SimpleLineIcons styleName='black' name='home' />);
    },
    screen: Screens.LoginScreen,
    naviTypes: [naviType.stack],
    children: []
  },
  {
    id: routesName.home.SettingsScreen,
    title: 'SettingsScreen',
    icon: (style) => {
      return (<SimpleLineIcons styleName='black' name='home' />);
    },
    screen: Screens.SettingsScreen,
    naviTypes: [naviType.stack],
    children: []
  }
];
export const MenuRoutes = _.cloneWith(MainRoutes, (value) => {
  if (_.indexOf(value.naviTypes, naviType.draw) >= 0) return value;
})
