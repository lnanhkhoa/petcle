import React from 'react';
import { connect } from 'react-redux';
import { 
    createStackNavigator, 
    createDrawerNavigator 
} from 'react-navigation';
import {
    reduxifyNavigator,
    createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';
import { Components } from '../../screens';
import { withTheme } from '../../config/themeProvider';
import { AppRoutes } from '../../config/navigation/routesBuilder';

// Note: createReactNavigationReduxMiddleware must be run before reduxifyNavigator
const naviMiddleware = createReactNavigationReduxMiddleware(
    "root",
    state => state.nav,
);

let SideMenu = withTheme(Components.SideMenu);
const RootNavigator = createStackNavigator({
    Home: {
        screen: createDrawerNavigator({
            ...AppRoutes,
        },
        {
            drawerOpenRoute: 'DrawerOpen',
            drawerCloseRoute: 'DrawerClose',
            drawerToggleRoute: 'DrawerToggle',
            contentComponent: (props) => <SideMenu {...props} />
        })
    }
}, { headerMode: 'none', });

const AppWithNavigationState = reduxifyNavigator(RootNavigator, "root");
const mapStateToProps = (state) => ({
    state: state.nav,
});
const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { RootNavigator, AppNavigator, naviMiddleware };