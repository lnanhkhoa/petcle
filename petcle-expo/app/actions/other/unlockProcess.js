const action = {
  UNLOCK_PROGRESS_CHANGE: {
    STRING: 'UNLOCK_PROGRESS_CHANGE'
  },

};

const creators = {
  unlockProgressChange: () => {
    return {
      type: action.UNLOCK_PROGRESS_CHANGE.STRING
    }
  }
}


export const act_unlock = {
  action,
  creators
}