const action = {
  START_SCAN : 'START_SCAN',
  STOP_SCAN : 'STOP_SCAN',
  DEVICE_FOUND : 'DEVICE_FOUND',
  CHANGE_DEVICE_STATE : 'CHANGE_DEVICE_STATE',
  WRITE_CHARACTERISTIC : 'WRITE_CHARACTERISTIC',
  READ_CHARACTERISTIC : 'READ_CHARACTERISTIC',
  MONITOR_CHARACTERISTIC : 'NOTIFY_CHARACTERISTIC',
  UPDATE_SERVICES : 'UPDATE_SERVICES',
  UPDATE_CHARACTERISTIC : 'UPDATE_CHARACTERISTIC',
  SELECT_SERVICE : 'SELECT_SERVICE',
  SELECT_CHARACTERISTIC : 'SELECT_CHARACTERISTIC',
  PUSH_ERROR : 'PUSH_ERROR',
  POP_ERROR : 'POP_ERROR',
  EXECUTE_TRANSACTION : 'EXECUTE_TRANSACTION',
  COMPLETE_TRANSACTION : 'COMPLETE_TRANSACTION',
  DEVICE_STATE_DISCONNECT : 'DISCONNECT',
  DEVICE_STATE_DISCONNECTING : 'DISCONNECTING',
  DEVICE_STATE_DISCONNECTED : 'DISCONNECTED',
  DEVICE_STATE_CONNECT : 'CONNECT',
  DEVICE_STATE_CONNECTING : 'CONNECTING',
  DEVICE_STATE_DISCOVERING : 'DISCOVERING',
  DEVICE_STATE_FETCHING : 'FETCHING SERVICES AND CHARACTERISTICS',
  DEVICE_STATE_CONNECTED : 'CONNECTED'
}

const creators = {
  startScan:()=>{
    return { type: action.START_SCAN };
  },

  stopScan() {
    return { type: action.STOP_SCAN };
  },

  deviceFound(device) {
    return { type: action.DEVICE_FOUND, device: device };
  },

  updateServices(deviceIdentifier, services) {
    return { type: action.UPDATE_SERVICES, deviceIdentifier: deviceIdentifier, services: services };
  },

  updateCharacteristic(deviceIdentifier, serviceUUID, characteristicUUID, characteristic) {
    return { type: action.UPDATE_CHARACTERISTIC, deviceIdentifier, serviceUUID, characteristicUUID, characteristic };
  },

  writeCharacteristic(deviceIdentifier, serviceUUID, characteristicUUID, base64Value) {
    return { type: action.WRITE_CHARACTERISTIC, deviceIdentifier: deviceIdentifier, serviceUUID: serviceUUID, characteristicUUID: characteristicUUID, base64Value: base64Value };
  },

  readCharacteristic(deviceIdentifier, serviceUUID, characteristicUUID) {
    return { type: action.READ_CHARACTERISTIC, deviceIdentifier: deviceIdentifier, serviceUUID: serviceUUID, characteristicUUID: characteristicUUID };
  },

  monitorCharacteristic(deviceIdentifier, serviceUUID, characteristicUUID, monitor) {
    return { type: action.MONITOR_CHARACTERISTIC, deviceIdentifier, serviceUUID, characteristicUUID, monitor };
  },

  changeDeviceState(deviceIdentifier, state) {
    return { type: action.CHANGE_DEVICE_STATE, deviceIdentifier: deviceIdentifier, state: state };
  },

  selectService(deviceIdentifier, serviceUUID) {
    return { type: action.SELECT_SERVICE, deviceIdentifier: deviceIdentifier, serviceUUID: serviceUUID };
  },

  selectCharacteristic(deviceIdentifier, serviceUUID, characteristicUUID) {
    return { type: action.SELECT_CHARACTERISTIC, deviceIdentifier: deviceIdentifier, serviceUUID: serviceUUID, characteristicUUID: characteristicUUID };
  },

  pushError(errorMessage) {
    return { type: action.PUSH_ERROR, errorMessage };
  },

  popError() {
    return { type: action.POP_ERROR };
  },

  executeTransaction(transactionId) {
    return { type: action.EXECUTE_TRANSACTION, transactionId };
  },

  completeTransaction(transactionId) {
    return { type: action.COMPLETE_TRANSACTION, transactionId };
  },
}

export const act_ble = {
  action, 
  creators
}
