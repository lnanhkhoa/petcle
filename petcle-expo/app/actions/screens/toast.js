const action = {
  SHOW_TOAST: 'SHOW_TOAST',
  CLEAR_TOAST: 'CLEAR_TOAST'
};

const creators = {
  show: (message, type) => {
    return {
      type: action.SHOW_TOAST,
      data: { message, type }
    }
  },
  clear: () => {
    return {
      type: action.CLEAR_TOAST
    }
  },
}

export const act_toast = {
  creators,
  action
}