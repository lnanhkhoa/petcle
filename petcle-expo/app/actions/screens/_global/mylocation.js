 const action = {
    REGION_CHANGE: 'MAP_REGION_CHANGE',
    GET_MY_LOCATION: {
        STRING: 'MAP_GET_MY_LOCATION',
        FETCH_SUCCESS: 'MAP_GET_MY_LOCATION_FETCH_SUCCESS',
        FETCH_FAIL: 'MAP_GET_MY_LOCATION_FETCH_FAIL'
    },// lấy toạ độ hiện tại của tôi
};

 const creators = {
    getMyLocation: (isFetchNearByMe) => {
        return {
            type: action.GET_MY_LOCATION.STRING,
            isFetchNearByMe
        }
    },
    mapRegionChange: (region) => {
        return {
            type: action.REGION_CHANGE,
            region
        }
    }
}

export const act_mylocation = {
    action,
    creators
}
