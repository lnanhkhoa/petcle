const action = {
  NAVIGATE: {
    CURRENT: 'NAVIGATE_CURRENT', // dinh danh screen,
    GOTO_SCREEN: 'NAVIGATE_GOTO_SCREEN',
    CLEAR_DATA_NAVIGATE: 'CLEAR_DATA_NAVIGATE'
  }
};

const creators = {
  setCurrentScreen: (data) => {
    return {
      type: action.NAVIGATE.CURRENT,
      data
    }
  },
  gotoScreen: (data) => {
    return {
      type: action.NAVIGATE.GOTO_SCREEN,
      data
    }
  },
  clearDataNavigate: (data) => {
    return {
      type: action.NAVIGATE.CLEAR_DATA_NAVIGATE,
      data
    }
  }

}


export const act_navigate = {
  action,
  creators
}

