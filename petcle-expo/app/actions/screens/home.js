 const action = {
    
    REFRESH_ELOCKER: 'HOME_REFRESH_ELOCKER',             // đưa về filter mặc định
    RELOAD_ELOCKER: 'HOME_RELOAD_ELOCKER',               // nạp lại danh sách elocker được lấy từ bằng fetch từ trước [no internet]
    DIRECTION_TO_ELOCKER: 'HOME_DIRECTION_TO_ELOCKER',   // chỉ đường từ toạ độ của tôi đến elocker
    IS_REFRESH_LOADING: 'HOME_IS_LOADING',               // Loading được bật
};

 const creators = {
    isRefreshing: (value) => {
        return {
            type: action.IS_REFRESH_LOADING,
            value
        }
    },
    fetchRefeshElocker: () => {
        return {
            type: action.REFRESH_ELOCKER
        }
    },
    reloadElocker: () => {
        return {
            type: action.RELOAD_ELOCKER
        }
    },
    directionToElocker: (lat, long) => {
        return {
            type: action.DIRECTION_TO_ELOCKER,
            lat: lat,
            long: long
        }
    }
}


export const act_home = {
    action,
    creators
}