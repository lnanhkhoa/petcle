 const action = {
    PROGRESS_CHANGE: {
        STRING: 'SPLASH_PROGRESS_CHANGE'
    }, 
    
};

 const creators = {
    progress_change: () => {
        return {
            type: action.PROGRESS_CHANGE.STRING
        }
    }
}


export const act_splash = {
    action,
    creators
}