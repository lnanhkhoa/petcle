const action = {
  CONNECT_USER_JOIN: 'connection/user_join',
  BOOKING_USER_STATUS_CHANGE: 'booking/user/status_changed',
  BOOKING_ELOCKER_STATUS_CHANGE: 'booking/elocker/status_changed',
  QRCODE_UNLOCK_ACTION: 'qrcode/unlock/action_package',
  QRCODE_UNLOCK_LOGIN: 'qrcode/unlock/login_qr_code',
  HARDWARE_WRITELINE: 'hardware/writeline'
};

const creators = {
  emitConnectUserJoin: (token_key) => {
    return {
      type: action.CONNECT_USER_JOIN,
      data: { user_authorization: token_key }
    }
  },
  eventBookingUserStatusChange: (data) => {
    return {
      type: action.BOOKING_USER_STATUS_CHANGE,
      data: data
    }
  },
  eventBookingElockerStatusChange: (data) => {
    return {
      type: action.BOOKING_ELOCKER_STATUS_CHANGE,
      data

    }
  },
  qrcodeUnlockAction: (data) => {
    return {
      type: action.QRCODE_UNLOCK_ACTION,
      data
    }
  },
  qrcodeUnlockLogin: (data) => {
    return {
      type: action.QRCODE_UNLOCK_LOGIN,
      data
    }
  },
  eventHardwareWriteline: (data) => {
    return {
      type: action.HARDWARE_WRITELINE,
      data
    }
  }
}

export const act_socket = {
  action,
  creators
}