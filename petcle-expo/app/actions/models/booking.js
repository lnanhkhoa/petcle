const action = {
    /**
    |--------------------------------------------------
    | ACTION GET ALL
    |--------------------------------------------------
    */
    GET_LOCKER_BY_USER: {
        STRING: 'GET_LOCKER_BY_USER_STRING',
        FETCH_SUCCESS: 'GET_LOCKER_BY_USER_FETCH_SUCCESS',
        FETCH_FAIL: 'GET_LOCKER_BY_USER_FETCH_FAIL',
    },
    UPDATE_CHECKIN_OUT_TIME: {
        STRING: 'UPDATE_CHECKINOUT_TIME_STRING',
        FETCH_SUCCESS: 'UPDATE_TIME_FETCH_TIME_SUCCESS',
        FETCH_FAIL: 'UPDATE_TIME_FETCH_TIME_FAIL',
    },
    ADD_BOOKING: {
        STRING: 'ADD_BOOKING_STRING',
        FETCH_SUCCESS: 'ADD_BOOKING_FETCH_SUCCESS',
        FETCH_FAIL: 'ADD_BOOKING_FETCH_FAIL',
    },
    UPDATE_BOOKING: {
        STRING: 'UPDATE_BOOKING_STRING',
        FETCH_SUCCESS: 'UPDATE_BOOKING_FETCH_SUCCESS',
        FETCH_FAIL: 'UPDATE_BOOKING_FETCH_FAIL',
    },
    CONFIRM_BOOKING: {
        STRING: 'CONFIRM_BOOKING_STRING',
        FETCH_SUCCESS: 'CONFIRM_BOOKING_FETCH_SUCCESS',
        FETCH_FAIL: 'CONFIRM_BOOKING_FETCH_FAIL'
    },
    GET_BOOKING_BY_USER_ID: {
        STRING: 'GET_BOOKING_BY_USER_ID_STRING',
        FETCH_SUCCESS: 'GET_BOOKING_BY_USER_ID_FETCH_SUCCESS',
        FETCH_FAIL: 'GET_BOOKING_BY_USER_ID_FETCH_FAIL'
    },
    GET_PACKAGE_BY_USER_ID: {
        STRING: 'GET_PACKAGE_BY_USER_ID_STRING',
        FETCH_SUCCESS: 'GET_PACKAGE_BY_USER_ID_FETCH_SUCCESS',
        FETCH_FAIL: 'GET_PACKAGE_BY_USER_ID_FETCH_FAIL'
    },
    PUT_BOOKING: {
        STRING: "PUT_BOOKING_STRING",
        FETCH_SUCCESS: "PUT_BOOKING_FETCH_SUCCESS",
        FETCH_FAIL: "PUT_BOOKING_FETCH_FAIL"
    },
    EXPORT_ELOCKER_BOOKING: {
        STRING: "EXPORT_ELOCKER_BOOKING_STRING",
        FETCH_SUCCESS: "EXPORT_ELOCKER_BOOKING_FETCH_SUCCESS",
        FETCH_FAIL: "EXPORT_ELOCKER_BOOKING_FETCH_FAIL"
    }
};

let updateBookingTest = {
    id: 322,
    note: "update",
}

let thisPagingLocker = {
    page: 1,
    page_size: 10,
};

const creators = {
    fetchGetLockerByUser: (paging) => {
        try {
            thisPagingLocker = { ...thisPagingLocker, paging };
            return {
                type: action.GET_LOCKER_BY_USER.STRING,
                paging: thisPagingLocker
            }
        } catch (e) {
            console.log(e);
        }

    },

    fetchLockerUserSuccess: (data) => {
        return {
            type: action.GET_LOCKER_BY_USER.FETCH_SUCCESS,
            data
        }
    },

    fetchLockerUserFail: (meta, error) => {
        return {
            type: action.GET_LOCKER_BY_USER.FETCH_FAIL,
            meta,
            error,
        }
    },

    fecthUpdateTimeCheckInOut: (time_update) => {
        try {
            return {
                type: action.UPDATE_CHECKIN_OUT_TIME.STRING,
                time_update: time_update,
            }
        } catch (e) {
            console.log(e);
        }
    },

    fetchAddBooking: (booking_data) => {
        try {
            return {
                type: action.ADD_BOOKING.STRING,
                booking_data: booking_data
            }
        } catch (e) {
            console.log(e);
        }
    },

    fetchUpdateBooking: (booking_data_update) => {
        try {
            return {
                type: action.UPDATE_BOOKING.STRING,
                booking_data: updateBookingTest,
            }
        } catch (e) {
            console.log(e);
        }
    },

    fetchBookingConfirm: (data) => {
        return {
            type: action.CONFIRM_BOOKING.STRING,
            data
        }
    },

    fetchBookingByUserId: (data) => {
        return {
            type: action.GET_BOOKING_BY_USER_ID.STRING,
            data
        }
    },
    fetchPackageByUserId: (data) => {
        return {
            type: action.GET_PACKAGE_BY_USER_ID.STRING,
            data
        }
    },
    fetchPutBooking: (data) => {
        return{
            type: action.PUT_BOOKING.STRING,
            data
        }
    },

    fetchExportElockerBooking: (data) => {
        return {
            type: action.EXPORT_ELOCKER_BOOKING.STRING,
            data
        }
    }
}

export const act_booking = {
    action,
    creators
}