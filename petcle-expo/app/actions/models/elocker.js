const action = {
    /**
    |--------------------------------------------------
    | ACTION GET PAGE
    | -'FETCH_SUCCESS', // sau khi request thành cồng thì nó làm cái quẹo gì ! (dispatch cái quẹo gì)
    | -'FETCH_FAIL', // sau khi request không được thì nó làm cái quẹo gì ! (dispatch cái quẹo gì)
    |--------------------------------------------------
    */
    GET_PAGE: {
        STRING: 'ELOCKER_GET_PAGE_STRING',
        FETCH_SUCCESS: 'ELOCKER_GET_PAGE_FETCH_SUCCESS',
        FETCH_FAIL: 'ELOCKER_GET_PAGE_FETCH_FAIL'
    },

    FILTER: {
        STRING: 'ELOCKER_GET_PAGE_STRING',
    },

    SORT: {
        STRING: 'ELOCKER_SORT_STRING',
    },

    SEARCH: {
        STRING: 'ELOCKER_SEARCH_STRING',
    },
    /**
    |--------------------------------------------------
    | ACTION GET BY ID
    |--------------------------------------------------
    */
    GET_BY_ID: {
        STRING: 'ELOCKER_GET_BY_ID_STRING',
        FETCH_SUCCESS: 'ELOCKER_GET_BY_ID_FETCH_SUCCESS',
        FETCH_FAIL: 'ELOCKER_GET_BY_ID_FETCH_FAIL'
    },
    ELOCKER_BOOKING_PUSH_ACTION: {
        STRING: 'ELOCKER_BOOKING_PUSH_ACTION',
        FETCH_SUCCESS: 'ELOCKER_BOOKING_PUSH_ACTION_FETCH_SUCCESS',
        FETCH_FAIL: 'ELOCKER_BOOKING_PUSH_ACTION_FETCH_FAIL',
    }
};

let thisPaging = {
    page: 1,
    page_size: 10,
    nbm_enable: false,
    nbm_current_lat: 10.7636565,
    nbm_current_long: 106.7005498,
    nbm_bounds_northeast_lat: 10.931858,
    nbm_bounds_northeast_long: 106.887834,
    nbm_bounds_southwest_lat: 10.704701,
    nbm_bounds_southwest_long: 106.461262,
    sort: "",
    set: "",
    key: "",
    elocker_type_id: "",
};

const creators = {

    fetchPageSuccess: (data) => {
        return {
            type: action.GET_PAGE.FETCH_SUCCESS,
            data
        }
    },
    fetchPageFail: (meta, error) => {
        return {
            type: action.GET_PAGE.FETCH_FAIL,
            meta,
            error,
        }
    },
    fetchElockerGetPage: (paging) => {
        thisPaging = { ...thisPaging, ...paging };
        return {
            type: action.GET_PAGE.STRING,
            paging: thisPaging,
            refreshing: true
        }
    },
    fetchFilterElocker: (elocker_type_id = thisPaging.elocker_type_id) => {
        thisPaging = { ...thisPaging, elocker_type_id }
        return {
            type: action.FILTER.STRING,
            paging: thisPaging,
            refreshing: true
        }
    },
    fetchSortElocker: (sort = thisPaging.sort) => {
        thisPaging = { ...thisPaging, sort }
        return {
            type: action.SORT.STRING,
            paging: thisPaging,
            refreshing: true
        }
    },
    fetchSortSetElocker: (set = thisPaging.set) => {
        thisPaging = { ...thisPaging, set }
        return {
            type: action.SORT.STRING,
            paging: thisPaging,
            refreshing: true
        }
    },
    fetchSearchElocker: (key = thisPaging.key) => {
        thisPaging = { ...thisPaging, key }
        return {
            type: action.SEARCH.STRING,
            paging: thisPaging,
            refreshing: true
        }
    },
    fetchGetById: (data) => {
        return {
            type: action.GET_BY_ID.STRING,
            data
        }
    },
    fetchElokerBookingPushAction: (data) => {
        return {
            type: action.ELOCKER_BOOKING_PUSH_ACTION.STRING,
            data
        }
    }
}

export const act_elocker = {
    action,
    creators
}