 const action = {
    SOCIAL_LOGIN: {
        STRING: 'LOGIN_SOCIAL_LOGIN',
        FETCH_SUCCESS: 'LOGIN_SOCIAL_LOGIN_FETCH_SUCCESS',
        FETCH_FAIL: 'LOGIN_SOCIAL_LOGIN_FETCH_FAIL'
    }
};

 const creators = {
    onFacebookLogin: () => {
        return {
            type: action.SOCIAL_LOGIN.STRING,
            social: 'facebook',
            login_loading: true
        }
    },
    onGoogleLogin: () => {
        return {
            type: action.SOCIAL_LOGIN.STRING,
            social: 'google',
            login_loading: true
        }
    },
    onUserLogin: (data) => {
        return {
            type: action.SOCIAL_LOGIN.STRING,
            social: 'server',
            login_loading: true,
            data
        }
    }
}

export const act_me = {
    action,
    creators
}