const action = {
  DATA_QRCODE_PUSH: {
    CREATE: 'CREATE_DATA_QRCODE_PUSH',
    REMOVE: 'REMOVE_DATA_QRCODE_PUSH',
  },
  DATA_QRCODE_LOGIN: {
    CREATE: "CREATE_DATA_QRCODE_LOGIN",
    REMOVE: "REMOVE_DATA_QRCODE_LOGIN"
  }
};

const creators = {
  createDataQrCodePush: (data) => {
    return {
      type: action.DATA_QRCODE_PUSH.CREATE,
      data
    }
  },
  removeDataQrCodePush: (data) => {
    return {
      type: action.DATA_QRCODE_PUSH.REMOVE,
      data
    }
  },
  createDataQrCodeLogin: (data) => {
    return {
      type: action.DATA_QRCODE_LOGIN.CREATE,
      data
    }
  },
  removeDataQrCodeLogin: (data) => {
    return {
      type: action.DATA_QRCODE_LOGIN.REMOVE,
      data
    }
  }

}

export const act_process_event = {
  action,
  creators
}