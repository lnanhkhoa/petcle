import _ from 'lodash'
import {SQLite} from 'expo';

class DataProvider {
	constructor(){
		this.db = SQLite.openDatabase('databooking.db');
		this.booking_table = 'booking_table'
    this.locker_booking_table = 'locker_booking_table'
  	this.package_table = 'package_table'
	}

	initial(){
		// db.transaction(tx => {
			// tx.executeSql(
			// 	'DROP TABLE IF EXISTS ?', 
			// 	[this.booking_table], (success)=>{
			// 		console.log('success');
			// 	}), (err)=>{
			// 	console.log('err');
			// })
		// });
	}

	dropAll(){
		db.transaction(tx => {
			tx.executeSql(
			  	'drop table if exists items'
			);
		});
	}
	createItems(){
		db.transaction(tx => {
			tx.executeSql(
				  'create table if not exists items (id integer primary key not null, done int, value text);'
			);
		});
	}
}

export const data = new DataProvider();