import React from "react";
import { StatusBar, View } from "react-native";
// import {
//     createDrawerNavigator,
//     createStackNavigator,
// } from 'react-navigation';
import { AppRoutes, DevRoutes } from "./config/navigation/routesBuilder";
import { Screens } from "./screens";

//import track from './config/analytics';
import { AppLoading, Font, Asset } from "expo";
import { RootNavigator } from "./config/navigation/appNavigator";
// import { Toast } from './screens/_global/toast'
import { Provider } from "react-redux";

import {Testing} from './test'


function cacheImages(images) {
  return images.map(image => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loaded: false
    };
  }

  _loadAssets = async () => {
    const imageAssets = cacheImages([
      //'https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png',
      require("./assets/images/splashBack.png")
    ]);

    const fontAssets = cacheFonts([
      {
        "Roboto-Bold": require("./assets/fonts/Roboto-Bold.ttf"),
        "Roboto-Medium": require("./assets/fonts/Roboto-Medium.ttf"),
        "Roboto-Regular": require("./assets/fonts/Roboto-Regular.ttf"),
        "Roboto-Light": require("./assets/fonts/Roboto-Light.ttf"),
        HelveticaNeue: require("./assets/fonts/helveticaneue.ttf"),
        "HelveticaNeue-Bold": require("./assets/fonts/helveticaneuebold.ttf"),
        "HelveticaNeue-Light": require("./assets/fonts/helveticaneuelight.ttf"),
        "rubicon-icon-font": require("@shoutem/ui/fonts/rubicon-icon-font.ttf"),
        "Rubik-Regular": require("@shoutem/ui/fonts/Rubik-Regular.ttf"),
        "SpaceMono-Bold": require("./assets/fonts/SpaceMono-Bold.ttf"),
        "SpaceMono-BoldItalic": require("./assets/fonts/SpaceMono-BoldItalic.ttf"),
        "SpaceMono-Italic": require("./assets/fonts/SpaceMono-Italic.ttf"),
        "SpaceMono-Regular": require("./assets/fonts/SpaceMono-Regular.ttf"),
        Helvetica: require("./assets/fonts/Helvetica.ttf")
      }
    ]);

    await Promise.all([...imageAssets, ...fontAssets]);
  };

  render() {
    StatusBar.setHidden(true);
    if (!this.state.loaded) {
      return (
        <AppLoading
          startAsync={this._loadAssets}
          onFinish={() => this.setState({ loaded: true })}
        />
      );
    }

    return (
      <Screens.VideoScreen/>
    );
  }
}
Expo.registerRootComponent(App);
