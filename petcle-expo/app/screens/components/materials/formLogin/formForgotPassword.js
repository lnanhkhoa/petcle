import React, { Component } from "react";
import {
  StyleSheet,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
  TouchableOpacity,
  Image,
  Dimensions
} from "react-native";

import { UserInput, ButtonSubmit, SignupSection } from ".";

import usernameImg from "../../../../assets/images/username.png";

export class FormForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <KeyboardAvoidingView
        behavior="padding"
        style={[styles.container, this.props.style]}
        enabled>
        <UserInput
          style={styles.textInput}
          source={usernameImg}
          placeholder="Username"
          autoCapitalize={"none"}
          returnKeyType={"done"}
          autoCorrect={false}/>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "transparent",
    alignItems: "center",
    justifyContent: "center"
  },
  textInput: {
    margin: 10
  },
  btnEye: {
    position: "absolute",
    top: 55,
    right: 28
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: "rgba(0,0,0,0.2)"
  }
});
