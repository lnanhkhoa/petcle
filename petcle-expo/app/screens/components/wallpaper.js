import React, {Component} from 'react';
import {StyleSheet, ImageBackground} from 'react-native';
// import { ImageBackground } from '@shoutem/ui';

export class Wallpaper extends Component {
  render() {
    let {
      background, children 
    } = this.props;
    return (
      <ImageBackground style={styles.picture} source={background}>
        {children}
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  picture: {
    flex: 1
  },
});
