import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';

export class VideoScreen extends Component {

  componentWillUnmount(){
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT);
  }

  componentDidMount() {
    Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.LANDSCAPE);
  }
  render() {
    return (
      <View style={styles.container}>
        <Text>1234</Text>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

