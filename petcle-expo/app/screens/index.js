
export const Screens = {
  ...require('./home'),
  ...require('./splash'),
  ...require('./login'),
  ...require('./settings'),  
  ...require('./video')

}

export const Components = {
  ...require('./components'),
  ...require('./_global')
}