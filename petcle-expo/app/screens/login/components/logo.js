import React, {Component} from 'react';
import {StyleSheet, View, Text, Image} from 'react-native';

export class Logo extends Component {
  render() {
    const {text, logoImage} = this.props;
    return (
      <View style={[this.props.style, styles.container]}>
        <Image source={logoImage} style={styles.image} />
        <Text style={styles.text}>{text}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 80,
    height: 80,
  },
  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 20,
  },
});
