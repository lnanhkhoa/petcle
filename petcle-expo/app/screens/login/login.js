import React, { Component } from "react";
import {
  StyleSheet,
  Image,
  Dimensions,
  KeyboardAvoidingView
} from "react-native";
import {
  Wallpaper,
  FormLogin,
  ButtonSubmit,
  SignupSection
} from "../components/";
import { Logo } from "./components/";

import bgSrc from "../../assets/images/dog-wallpaper.png";
import logoSrc from "../../assets/images/logo.png";

export class LoginScreen extends Component {
  render() {
    return (
      <Wallpaper background={bgSrc}>
        <Logo style={styles.logo} text={"React Native"} logoImage={logoSrc} />
        <KeyboardAvoidingView behavior="padding" style={styles.viewform}>
          <FormLogin style={styles.formLogin} />
          <ButtonSubmit
            text={"LOGIN"}
            // style={styles.buttonLogin}
            // styleButton={styles.buttonLogin}
            // color={"#a55eea"}
          />
          <SignupSection />
        </KeyboardAvoidingView>
      </Wallpaper>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    flex: 1
    // marginTop: 80,
    // marginBottom: 40
  },
  viewform: {
    flex: 1
  },
  formLogin: {
    // margin: 10
  },
  buttonLogin: {
    backgroundColor: "#a55eea"
    // marginTop: 40
  }
});
