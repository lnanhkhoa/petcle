import React, { Component } from "react";
import { View, Text, StyleSheet, KeyboardAvoidingView } from "react-native";
import {
  Wallpaper,
  FormForgotPassword,
  ButtonSubmit,
  SignupSection
} from "../../components/";
import { Logo } from "../components/";

import bgSrc from "../../../assets/images/splash-petcle.png";
import logoSrc from "../../../assets/images/logo.png";

export class ForgotScreen extends Component {
  render() {
    return (
      <Wallpaper background={bgSrc}>
        <Logo style={styles.logo} text={"React Native"} logoImage={logoSrc} />
        <KeyboardAvoidingView behavior="padding" style={styles.viewform}>
          <FormForgotPassword style={styles.form} />
          <ButtonSubmit text={"SEND"} style={styles.buttonSend} />
          <ButtonSubmit
            text={"BACK"}
            styleButton={styles.buttonBack}
            color={"#7f8c8d"}
          />
        </KeyboardAvoidingView>
      </Wallpaper>
    );
  }
}

const styles = StyleSheet.create({
  viewform: {
    flex: 1
  },
  logo: {
    flex: 1
    // marginTop: 80
  },
  form: {
    // marginTop: 100
  },
  buttonSend: {
    // marginTop: 30
  },
  buttonBack: {
    backgroundColor: "#7f8c8d"
  }
});
