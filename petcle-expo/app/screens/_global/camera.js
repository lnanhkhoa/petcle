import React from 'react';
import { Alert } from 'react-native';
import { Camera, Permissions } from 'expo';
import { Ionicons } from '../_global/Icons';
import { View,Text,Screen,Title } from '@shoutem/ui';

export class CameraComponent extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return (
            {
                title: 'Scan QRCode',
            });
    };

    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
    };

    async componentWillMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }
    onBarCodeRead(type, data) {
        Alert.alert(data);
    }
    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <View />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <Screen>
                    <Camera style={{ flex: 1 }} type={this.state.type}>
                        <View styleName='vertical h-center md-gutter-horizontal flexible'>
                            <View styleName='md-gutter-vertical'>
                                <Text styleName='white' >Scan QR code để sử dụng Locker</Text>
                            </View>
                            <View styleName='md-gutter-vertical flexible'></View>
                            <View styleName='md-gutter-vertical lg-gutter-bottom'>
                                <Ionicons styleName='xxxl-size white' name='md-bulb' />
                            </View>
                        </View>
                    </Camera>
                </Screen>
            );
        }
    }
}