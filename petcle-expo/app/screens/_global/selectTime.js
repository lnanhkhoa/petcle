
import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet, ScrollView, Slider } from 'react-native';
import { Ionicons } from '../_global/Icons';
import { theme } from '../../config/themeProvider';
import {
    Screen, View, Text, Row, Button, Heading,
    Caption, Subtitle, Title, Image, ListView, ImageBackground, Tile, Divider
} from '@shoutem/ui';
import { routesName } from '../../config/navigation/routes';
import DateTimePicker from 'react-native-modal-datetime-picker';
import { FormCheckInOut } from '../../screens/_global/formCheckInOut';
import _ from 'lodash';
import { lang } from '../../../node_modules/moment';
import {act_booking} from '../../actions/models/booking';

export class SelectTimeComponent extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return (    
            {
                title: 'Select Time',
            });
    };

    constructor(props) {
        super(props);
        this.state = {
            check_in: new Date(),
            check_out: new Date(),
        }
    }

    handleChangeStartDate = (date) => {
        this.setState({check_in : date});
    }

    handleChangeEndDate = (date) => {
        this.setState({check_out : date});
    }

    render() {
        const mapDispatchToProps = (dispatch) => {
            return {
                onUpdateTimeCheckInOut: () => {
                    dispatch(act_booking.creators.fecthUpdateTimeCheckInOut())
                },
            }
        }
        
        return (
            <Screen styleName='md-gutter space-between'>
                <View styleName='sm-margin-bottom shadow horizontal paper'>
                    <FormCheckInOut
                        onSelectChangeStartDate={this.handleChangeStartDate}   
                        onSelectChangeEndDate={this.handleChangeEndDate}
                    />
                </View>
                <View>
                    <Button
                        styleName='elocker'
                        onPress={() => {
                            this.props.navigation.goBack();
                            this.props.navigation.state.params.returnDataTime(this.state.check_in, this.state.check_out);
                        }}
                    >
                        <Text>Done</Text>
                    </Button>
                </View>
            </Screen>
        );
    }
}


