import React, { Component } from 'react'
import {BoxShadow} from 'react-native-shadow'
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'
import {theme} from '../../config/themeProvider';
import {FontAwesome} from '@expo/vector-icons'; 


export class FormCheckin extends React.Component{
  render(){
    var calculator_time = diff(this.props.timefrom, this.props.timeto); 

    const shadowOpt = {
			width: 340,   
      height: 112,
			color:"#212121",
			border:2, 
			radius:1, 
			opacity:0.5,
			x:0,
			y:0,
			style:{marginVertical:5}
    }
    
    return (
      <BoxShadow setting={shadowOpt}>
        <View style={styles.container}>
          <View style={styles.block1}>
            <Text style={styles.row1}>Check-in{'\n'}</Text>
            <Text style={styles.row2}>{this.props.datefrom}{'\n'}</Text>
            <Text style={styles.row3}>{this.props.timefrom}{'\n'}</Text>
          </View>
          <View style={styles.block2}>
            <FontAwesome name='arrow-right' size={theme["shoutem.ui.Icon"].fontSize} />
            <Text style={styles.time}>{calculator_time}</Text>
          </View>
          <View style={styles.block3}>
            <Text style={styles.row1}>Check-out{'\n'}</Text>
            <Text style={styles.row2}>{this.props.dateto}{'\n'}</Text>
            <Text style={styles.row3}>{this.props.timeto}{'\n'}</Text>
          </View>
        </View>
      </BoxShadow>
    );
  }
}

// set style for component
const styles = StyleSheet.create({
  container: {
    width: 340, 
    height: 112,
    backgroundColor: "#ffffff",
    flex: 1,
    flexDirection: 'row',
    padding: 5,
  },
  // design follow column
  block1: { 
    flex: 35,
    justifyContent: 'center',
    alignItems: 'center',  
    flexDirection: 'column',
  },
  block2: {
    flex: 30,
    paddingTop: 50,
    alignItems: 'center',
    flexDirection: 'column',
  },
  block3: {
    flex: 35,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },

  // design follow row 
  row1: {
    fontSize: 12,
    color: "#b9b9b9",
    fontFamily: "Helvetica"
  },
  row2: {
    fontSize: 13,
    color: "#3f3f3f", 
    paddingTop: 10,
    fontFamily: "Helvetica"
  },
  row3: {
    fontSize: 28,
    color: "#3f3f3f",
    fontFamily: "Helvetica"
  },  
  icon: {
    fontSize: 20,
    color: 'black',
  },
  time: {
    fontSize: 13,
    color: 'black',
    fontFamily: "Helvetica" 
  }
})

// calculator time beetween two time
function diff(start, end) {
  start = start.split(":");
  end = end.split(":");
  var startDate = new Date(0, 0, 0, start[0], start[1], 0);
  var endDate = new Date(0, 0, 0, end[0], end[1], 0);
  var diff = endDate.getTime() - startDate.getTime();
  var hours = Math.floor(diff / 1000 / 60 / 60);
  diff -= hours * 1000 * 60 * 60;
  var minutes = Math.floor(diff / 1000 / 60);

  // If using time pickers with 24 hours format, add the below line get exact hours
  if (hours < 0)
     hours = hours + 24;

  return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}


