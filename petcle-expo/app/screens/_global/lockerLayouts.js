import React, { Component } from 'react'
import {BoxShadow} from 'react-native-shadow'
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  TouchableHighlight,
  View
} from 'react-native'
import { theme } from '../../config/themeProvider';  
import {FontAwesome, Feather} from '@expo/vector-icons'; 


export class FormElocker extends React.Component{
    render(){
        var calculator_time = diff(this.props.timefrom, this.props.timeto); 

        const shadowOpt = {
                width: 340,   
                height: 230,        
                color:"#212121",
                border:2,   
                radius:1, 
                opacity:0.5,
        }
        
        return (
            <BoxShadow setting={shadowOpt}>
                <View style={styles.header}>
                    <View style={styles.header_block1}>
                        <Text style = {{fontFamily: "Helvetica", fontSize: 15}}>
                            Locker - Tòa nhà H2{'\n'}
                        </Text>
                        <Text style = {{fontFamily: "Helvetica"}}>1 Size S 2 Size L</Text>
                    </View>    
                    <View style={styles.header_block2}> 
                        <TouchableHighlight onPress={()=>{}}>
                            <View>
                                <FontAwesome name='chevron-right' size={theme["shoutem.ui.Icon"].fontSize} />
                            </View>
                        </TouchableHighlight>   
                    </View> 
                </View>
                <View style={styles.container}>     
                    <View style={styles.block1}>
                        <Text style={styles.row1}>Check-in{'\n'}</Text>
                        <Text style={styles.row2}>{this.props.datefrom}{'\n'}</Text>
                        <Text style={styles.row3}>{this.props.timefrom}{'\n'}</Text>
                    </View>
                    <View style={styles.block2}>
                        <FontAwesome name='arrow-right' size={theme["shoutem.ui.Icon"].fontSize} />
                        <Text style={styles.time}>{calculator_time}</Text>
                    </View>
                    <View style={styles.block3}>
                        <Text style={styles.row1}>Check-out{'\n'}</Text>
                        <Text style={styles.row2}>{this.props.dateto}{'\n'}</Text>
                        <Text style={styles.row3}>{this.props.timeto}{'\n'}</Text>
                    </View> 
                </View>
                <View style={styles.footer}>
                    <View style={styles.footer_block1}>
                        <TouchableHighlight onPress={()=>{}} style={styles.ButtonLeft}>
                            <View>
                                <Text style = {{
                                    width: 121,
                                    height: 19,
                                    fontFamily: "Helvetica",
                                    fontSize: 16,
                                    color: 'white'
                                }}>
                                    Thêm thời gian
                                </Text>
                            </View> 
                        </TouchableHighlight>     
                    </View>    
                    <View style={styles.footer_block2}>     
                            <TouchableHighlight onPress={()=>{}} style={styles.ButtonRight}>
                                <View style={styles.footer_item}>
                                    <View style={styles.footer_item1}>
                                        <Feather name='maximize' size={theme["shoutem.ui.Icon"].fontSize} style = {{color : 'white'}} />
                                    </View> 
                                    <View style={styles.footer_item2}>
                                        <Text style = {{
                                            width: 121,
                                            height: 19,
                                            fontFamily: "Helvetica",
                                            fontSize: 16,
                                            color: 'white'
                                        }}>
                                            Mở khóa
                                        </Text>     
                                    </View> 
                                </View>
                            </TouchableHighlight>  
                    </View> 
                </View>
            </BoxShadow>
        );
    }
}

// set style for component  
const styles = StyleSheet.create({
    // design header
    header: {
        width: 340, 
        height: 20,
        backgroundColor: "#ffffff", 
        flex: 1,    
        flexDirection: 'row',
    },
    header_block1: {
        flex: 80,
        padding: 10,
    },
    header_block2: {
        flex: 20,
        alignItems: 'center',  
        paddingTop: 10,
    },

    // design body
    container: {
        width: 340, 
        height: 127,    
        backgroundColor: "#ffffff",
        flex: 1,        
        flexDirection: 'row',
        padding: 5,
    },
    block1: { 
        flex: 35,
        justifyContent: 'center',
        alignItems: 'center',  
        flexDirection: 'column',    
    },
    block2: {
        flex: 30,
        paddingTop: 40, 
        alignItems: 'center',   
        flexDirection: 'column',
    },
    block3: {
        flex: 35,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
    },

    // design footer
    footer: {
        width: 340, 
        height: 20,
        backgroundColor: "#ffffff", 
        flex: 1,    
        flexDirection: 'row',
    },
    footer_block1: {
        flex: 50,
        alignItems: 'center',  
        justifyContent: 'center',    
    },
    footer_block2: {
        flex: 50,
        alignItems: 'center',  
        justifyContent: 'center',   
    },
    footer_item: {
        flex: 1,   
        width: 148, 
        flexDirection: 'row',
    },
    footer_item1: {
        flex: 20,
        paddingLeft: 5,
    },  
    footer_item2: {
        flex: 80,
        paddingLeft: 5,
    },

    // design follow row 
    row1: {
        color: "#b9b9b9",
        fontFamily: "Helvetica",
        fontSize: 11,
        fontWeight: "normal",
        fontStyle: "normal"
    },
    row2: {
        color: "#3f3f3f", 
        paddingTop: 10,
        fontFamily: "Helvetica",
        fontSize: 11,
        fontWeight: "normal",
        fontStyle: "normal",
    },
    row3: {
        fontSize: 28,
        color: "#3f3f3f",
        fontFamily: "Helvetica"
    },
    icon: { 
        fontSize: 20,
        color: 'black',
    },
    time: {
        fontSize: 13,   
        color: 'black',
        fontFamily: "Helvetica"
    },  
    ButtonLeft: {
        margin: 15,
        width: 148.5,
        height: 40,
        backgroundColor: '#581247', 
        padding: 10,
        alignItems: 'center',
        borderRadius: 2,
    },
    ButtonRight: {
        margin: 15,
        width: 148.5,
        height: 40,
        backgroundColor: '#14672f', 
        padding: 10,
        alignItems: 'center',
        borderRadius: 2,
    }
})

// calculator time beetween two time
function diff(start, end) {
  start = start.split(":");
  end = end.split(":");
  var startDate = new Date(0, 0, 0, start[0], start[1], 0);
  var endDate = new Date(0, 0, 0, end[0], end[1], 0);
  var diff = endDate.getTime() - startDate.getTime();
  var hours = Math.floor(diff / 1000 / 60 / 60);
  diff -= hours * 1000 * 60 * 60;
  var minutes = Math.floor(diff / 1000 / 60);

  // If using time pickers with 24 hours format, add the below line get exact hours
  if (hours < 0)
     hours = hours + 24;

  return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
}


