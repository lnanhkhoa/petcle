import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { theme } from '../../config/themeProvider';
import {    
    Screen, View, Text, Row, 
    Icon, Button, Heading, Caption, 
    Subtitle, Image, TextInput, Title,
} from '@shoutem/ui';
import { StyleProvider } from '@shoutem/theme';
import _ from 'lodash';

export class LockDetailComponent extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return (
            {
                title: 'Lock Detail',
            });
    };

    constructor(props) {
        super(props);   
    }   
    render() {
        return (    
            <Screen styleName='md-gutter paper'>  
                <View styleName='vertical space-between flexible'>
                    <View>       
                        <View styleName='lg-margin-bottom center'>       
                            <Heading style={{fontWeight: 'bold',color: '#4a4a4a',}}>Lock #109323235 (Size M)</Heading>
                            <Title style={{color: '#14672f',}}>Thời gian còn lại : 15h30 phút</Title>
                        </View>                 
                        <View styleName='xl-margin-bottom'>
                            <Text>Ghi chú của bạn</Text>
                            <TextInput styleName='multiline shadow'>
                                - 1 Bọc màu đen - Điện thoại....    
                            </TextInput>
                        </View>             
                        <View>              
                            <Text styleName='xl-size' style={{fontFamily: "Helvetica"}} >Mã sử dụng tại địa điểm</Text>   
                            <Row styleName="m-small shadow">
                                <Icon name="photo" />  
                                <Text style={{fontWeight: 'bold', color: '#581247'}}>TX0935235235</Text>
                                <Text styleName='xl-margin-left'>   00:30</Text>
                            </Row>
                        </View> 
                    </View>

                    <View>  
                        <View styleName='center'>
                            <Text styleName='xl-size' >Hỗ trợ: 098 20202 575</Text>
                        </View> 
                        <View styleName="horizontal lg-gutter border-topColor border-topWidth">
                            <Button styleName="sm-gutter elocker flexible">
                                <Ionicons styleName='white' name='ios-alarm-outline' />
                                <Text>Gia hạn</Text>
                            </Button>
                            <Button styleName="sm-gutter success flexible" >
                                <Ionicons styleName='white' name='ios-qr-scanner-outline' />
                                <Text>Mở khoá</Text>
                            </Button>
                        </View>
                    </View>
                </View>
            </Screen>
        );
    }
}