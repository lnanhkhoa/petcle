
import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet, ScrollView, Slider, FlatList } from 'react-native';
import { Ionicons, SimpleLineIcons } from '../_global/Icons';
import { theme } from '../../config/themeProvider';
import {
    Screen, View, Text, Row, Icon, Button, Heading, Image,
    Caption, Subtitle, Title, ListView, ImageBackground, Tile, Divider, Html
} from '@shoutem/ui';
import { connectStyle } from '@shoutem/theme';
import { connect } from 'react-redux';
import * as act_lockertype from '../../actions/models/lockerTypes';

class FlatListItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeRowKey: null,
            clicks: 0,
        }
    }

    // handle incre and decre when you click button
    IncrementItem = () => {
        this.setState(
            { clicks: this.state.clicks + 1 }
        );
    }
    DecreaseItem = () => {
        this.setState(
            { clicks: this.state.clicks - 1 }
        );
    }

    render() {
        let renderCustomLockerSize = (row) => {
            if (row.isCustom == true) {
                return (<Subtitle>(40x40x<Subtitle>10</Subtitle>)</Subtitle>);
            }
        }
        let renderCustomLockerSlider = (row) => {
            if (row.isCustom == true) {
                return (<Slider minimumValue={5} maximumValue={100} value={40} minimumTrackTintColor={theme._variables.featuredColor} ></Slider>);
            }
            else {
                return (<Caption numberOfLines={1}>{row.description}</Caption>);
            }
        }

        const row = this.props.item;
        return (
            <Row key={row.id}>
                <View styleName="vertical" style={styles.root.elocker.lockerTypeRow}>
                    <View styleName="horizontal v-center space-between">
                        <Title>{row.name}</Title>
                        {renderCustomLockerSize(row)}
                    </View>
                    {renderCustomLockerSlider(row)}
                    <Subtitle numberOfLines={1}>{
                        row.perHour + row.perDay <= 0 ?
                            `Hệ thông đang kiểm tra ...` :
                            `Giá : ${row.price.perHour}đ / giờ - ${row.price.perDay}đ / 24h`
                    }</Subtitle>
                    <View styleName="horizontal v-center space-between">
                        <Caption styleName='flexible italicTextStyle' style={row.avaiableNumberLock > 0 ? { color: theme._variables.success } : { color: theme._variables.accent }} numberOfLines={1}>{
                            row.avaiableNumberLock == -1 ? `hệ thống đang kiểm tra` :
                                row.avaiableNumberLock == 0 ? `Hết chỗ trống tại thời điểm đặt` :
                                    `Trống ${row.avaiableNumberLock} lock`
                        }
                        </Caption>
                        <View styleName="horizontal v-center space-between">
                            <Button onPress={this.DecreaseItem} disabled={(this.state.clicks <= 0) ? true : false}>
                                <Ionicons
                                    style={{ color: (this.state.clicks <= 0) ? theme._variables.overlay : theme._variables.selected }}
                                    name='ios-remove-circle'
                                />
                            </Button>
                            <Text>{this.state.clicks}</Text>
                            <Button onPress={this.IncrementItem} disabled={(this.state.clicks >= 1) ? true : false}>
                                <Ionicons
                                    style={{ color: (this.state.clicks >= 1) ? theme._variables.overlay : theme._variables.selected }}
                                    name='ios-add-circle'
                                />
                            </Button>
                        </View>
                    </View>
                    <Divider styleName="line" />
                </View>
            </Row>
        );
    }
}

class _SizeListViewComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        //this.props.onLockerTypeGetInfo();
    }

    render() {
        const data_exp = this.props.lockertypes.data;
        return (
            <FlatList
                data={data_exp}
                renderItem={({ item, index }) => {
                    return (
                        <FlatListItem item={item} index={index} parentFlatList={this}>

                        </FlatListItem>);
                }}
            />
        );
    }
}   

const mapStateToProps = (state) => {
    return { lockertypes: state.lockertypeReducers }
}
const mapDispatchToProps = (dispatch) => {
    return {
        onLockerTypeGetInfo: () => {
            dispatch(act_lockertype.creators.fetchGetLockertypeInfo())
        },
    }
}
const SizeListViewComponent = connect(mapStateToProps, mapDispatchToProps)(_SizeListViewComponent);
export { SizeListViewComponent }

let styles = {
    root: {
        maps: {
            height: 170
        },
        elocker: {
            image: {
                marginTop: -50,
                width: 80
            },
            lockerTypeRow: {
                marginBottom: 0,
            }
        }
    }
}
