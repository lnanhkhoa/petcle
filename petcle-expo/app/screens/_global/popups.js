import React, { Component } from 'react';
import { StyleSheet, Dimensions, Alert, Picker } from 'react-native';
import { Ionicons } from './Icons';
import { theme } from '../../config/themeProvider';
import {
    View, Text, Button,
    Subtitle, Divider
} from '@shoutem/ui';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import DropdownMenu from './dropdown';
import _ from 'lodash';

let { width } = Dimensions.get('window');

class PopupFilterSearchElockerComponent extends Component {
    constructor(props) {
        super(props);
        this.sortby_data = [[
            { key: null, label: 'chọn thứ tự xắp xếp' },
            { key: 'near_by_me', label: 'Gần tôi' },
            { key: 'space', label: 'Còn nhiều chỗ trống' },
            { key: 'recommend', label: 'Gợi ý' },
            { key: 'optimize', label: 'Tối ưu nhất' },
        ],
        [
            { key: 'desc', label: 'A->z' },
            { key: 'asc', label: 'Z->a' },
        ]];
        this.filter_data = [
            { key: 1, label: 'Building' },
            { key: 2, label: 'Cửa hàng tiện ích' },
            { key: 3, label: 'Ngoài trời' },
        ];
    }

    setDialog = (popupDialog) => {
        this.props.onSetDialog(popupDialog);
    };

    onSelectFilter = (item) => {
        this.props.onSelectFilter(item);
    };

    onSelectSort = (selection, item) => {
        this.props.onSelectSort(selection, item);
    };

    render() {
        return (
            <PopupDialog
                dialogStyle={{ borderRadius: 0, position: 'absolute', bottom: 0 }}
                height={0.7}
                dialogAnimation={new SlideAnimation({ slideFrom: 'bottom' })}
                ref={this.setDialog}>
                <Subtitle styleName='md-gutter 0-margin-bottom h-center bold'>Sort and Filter</Subtitle>
                <View styleName='vertical v-center md-gutter'>
                    <View styleName='vertical md-gutter-bottom'>
                        <Subtitle styleName='sm-gutter bold'>Filter</Subtitle>
                        <View styleName='horizontal'>
                            {this.filter_data.map((item, index) => {
                                return (
                                    <Button key={item.key} styleName='sm-gutter' style={styles.btnSubFilter} onPress={() => { this.onSelectFilter(item) }}>
                                        <Text styleName='center' numberOfLines={2} >{item.label}</Text>
                                    </Button>
                                );
                            })}
                        </View>
                    </View>
                    <Divider styleName='line' />
                    <View styleName='vertical sm-gutter-top' >
                        <Subtitle styleName='sm-gutter bold'>Sort By</Subtitle>
                        <DropdownMenu
                            style={{ flex: 1 }}
                            bgColor={'white'}
                            tintColor={'#666666'}
                            activityTintColor={'green'}
                            // arrowImg={}      
                            // checkImage={}   
                            optionTextStyle={{ color: '#333333' }}
                            titleStyle={{ color: '#333333' }}
                            maxHeight={300}
                            handler={(selection, row) => { this.onSelectSort(selection, this.sortby_data[selection][row]) }}
                            data={[_.map(this.sortby_data[0], 'label'), _.map(this.sortby_data[1], 'label')]}>
                        </DropdownMenu>

                    </View>
                </View>
            </PopupDialog>
        );
    }
}

export { PopupFilterSearchElockerComponent }

let styles = {
    btnSubFilter: {
        width: (width - 60) / 3,
        borderRadius: 5,
        height: 60,
        borderStyle: 'solid',
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: theme._variables.elockerButtonBorderColor
    },
    btnSubSort: {
        position: 'relative',
        flex: 1,
        height: 48,
        borderRadius: 5,
        borderStyle: 'solid',
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: theme._variables.indicatorColor
    },
}