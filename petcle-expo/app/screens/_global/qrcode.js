import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';
import QRCode from 'react-native-qrcode'; // 0.2.6

export class QRCodeComponent extends Component {
  constructor(props){
    super(props);
    this.state = {
      content: 'elocker',
      width: null,
      padding: null
    }
  }
  componentWillReceiveProps(newprops){
    if(!!newprops.width && !! newprops.padding){
      this.state.width = newprops.width;
      this.state.padding = newprops.padding; 
    }
    if(!!newprops.content){
      this.state.content = newprops.content
    }
  }

  render() {
    return (
      <View 
        style={[
          styles.container, 
          {padding: this.state.padding,            
          }
      ]}>
        <QRCode
          value={this.state.content}
          size={this.state.width}
          bgColor='black'
          fgColor='white'
        />

      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#000',
    padding: 5,
    marginBottom: 5
  }
});