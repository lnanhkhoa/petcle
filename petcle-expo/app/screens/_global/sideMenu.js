import React from 'react';
import {
  StyleSheet, StatusBar, Alert, Row as LineAround
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import { LinearGradient } from 'expo';
import { MenuRoutes } from '../../config/navigation/routes';
import { Ionicons } from '../_global/Icons';
import { hexToRGBA, setStatusBarStyle } from '../../helper/colorUtils';
import { UIConstants } from '../../config/appConstants';
import {
  Row, Title, TouchableOpacity, Screen, Text, Card, Image, Subtitle, Button,
  Heading, ScrollView, ListView, Caption, View, Divider
} from '@shoutem/ui';
import { theme } from '../../config/themeProvider';


export class SideMenu extends React.Component {

  constructor(props) {
    super(props);
    this._navigateAction = this._navigate.bind(this);
    this.renderRow = this.renderRow.bind(this);
  }

  componentWillMount() {
    setStatusBarStyle('black');
  }

  _navigate(route) {
    let resetAction = StackActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: route.id })
      ]
    });
    this.props.navigation.dispatch(resetAction)
  }

  _renderIcon() {
    return <Image
      styleName="small-avatar"
      source={require('../../assets/images/avatar.jpg')}
    />
  }

  renderRow(row, index) {
    return (
      <Button onPress={() => this._navigateAction(row)} key={index} styleName='clear 0-padding'>
        <Row styleName="small clear">
          {row.icon()}
          <Title styleName='black flexible clear'>{row.title}</Title>
          {/* <Ionicons styleName='white disclosure' name="ios-arrow-forward-outline" /> */}
        </Row>
      </Button>
    )
  }

  render() {
    let menuData = MenuRoutes

    return (
      <Screen styleName='md-gutter horizontal gray-bg'>
        <View styleName='vertical'>
          <Card styleName='clear'>
            <View style={styles.avatar.boder1} styleName='clear'>
              <View style={styles.avatar.boder2} styleName='clear'>
                <View style={styles.avatar.boder3} styleName='clear'>
                  {this._renderIcon()}
                </View>
              </View>
            </View>

            <View styleName='content clear'>
              <Subtitle styleName='black bold'>Khoa Le</Subtitle>
              <Caption>21 hours ago</Caption>
            </View>
          </Card>
          <Divider styleName="line" />
          <ListView
            showsVerticalScrollIndicator={false}
            styleName='clear'
            data={menuData}
            renderRow={this.renderRow} />
        </View>
      </Screen>
    )
  }
}
const styles = {
  root: {
    start: theme.featuredBackgroundGradients.base.start,
    end: theme.featuredBackgroundGradients.base.end,
    locations: theme.featuredBackgroundGradients.base.locations,
    colors: theme.featuredBackgroundGradients.base.colors,
  },
  container: StyleSheet.create({
    root: {
      flex: 1,
      //flexDirection: 'column',
      paddingTop: UIConstants.StatusbarHeight,
    },
  }),
  avatar: {
    content: {
      borderBottomColor: theme._variables.navBarBorderColor,
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    boder1: {
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: hexToRGBA(theme._variables.navBarBorderColor, 0.1),
      borderRadius: 55,
      width: 110,
      height: 110,
      justifyContent: 'flex-end',
      flexDirection: 'row',
      alignItems: 'flex-end',
      paddingLeft: 4,
      paddingBottom: 1,
      paddingRight: 1,
      paddingTop: 4,
    },
    boder2: {
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: hexToRGBA(theme._variables.navBarBorderColor, 0.2),
      borderRadius: 50,
      width: 100,
      height: 100,
      justifyContent: 'center',
      flexDirection: 'row',
      alignItems: 'center',
    },
    boder3: {
      borderWidth: StyleSheet.hairlineWidth,
      borderColor: hexToRGBA(theme._variables.navBarBorderColor, 0.5),
      borderRadius: 40,
      width: 80,
      height: 80,
      justifyContent: 'center',
      flexDirection: 'row',
      alignItems: 'center',
    },
    image: {
      borderColor: hexToRGBA(theme._variables.navBarBorderColor, 0.5),
      borderRadius: 30,
      width: 60,
      height: 60,
      justifyContent: 'center',
      flexDirection: 'row',
      alignItems: 'center'
    }
  }
}