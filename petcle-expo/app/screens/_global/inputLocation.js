import React, { Component } from 'react'
import {BoxShadow} from 'react-native-shadow'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import {
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'   
import { theme } from '../../config/themeProvider';
import {SimpleLineIcons, FontAwesome} from '@expo/vector-icons'; 

// input location layout
export class InputLocation extends React.Component{
    constructor (props, ctx) {
        super(props, ctx);

        this.state = {
          visible: false,
          picked: null,
        };
    }

    render(){
        const shadowInput = {
            width: 340,
            height: 48,
            color:"#212121",
            border:2,   
            radius:1, 
            opacity:0.5,
            x:0,
            y:0,
            style:{marginVertical:5}
        };  

        const { visible, picked } = this.state;
        const options = [
            {
                key: 'kenya',
                label: 'Kenya',
            },
            {
                key: 'uganda',
                label: 'Uganda',
            },
            {
                key: 'libya',
                label: 'Libya',
            },
            {
                key: 'morocco',
                label: 'Morocco',
            },
            {
                key: 'estonia',
                label: 'Estonia',
            },
        ];

        return (
        <BoxShadow setting={shadowInput}>
            <View style={styles.containerInput}>
                <View style={styles.blockInput1}>
                    <SimpleLineIcons name   =   'location-pin' 
                        size    =   {theme["shoutem.ui.Icon"].fontSize}
                        style   =   {styles.icon1} 
                    />
                </View>             
                <View style={styles.blockInput2}>                   
                    <TextInput 
                        underlineColorAndroid='transparent'
                        onChangeText={(picked) => this.setState({picked})}
                        value={picked} 
                    />
                </View>
                <View style={styles.blockInput3}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={this.onShow}>
                        <FontAwesome name='location-arrow' size={theme["shoutem.ui.Icon"].fontSize} />
                    </TouchableOpacity>      
                    <ModalFilterPicker
                        visible={visible}
                        onSelect={this.onSelect}
                        onCancel={this.onCancel}
                        options={options}           
                    />
                </View> 
            </View>
        </BoxShadow>
        );
    }

    onShow = () => {
        this.setState({ visible: true });
    }
    onSelect = (picked) => {
        this.setState({
            picked: picked,
            visible: false           
        })
    }
    onCancel = () => {
        this.setState({ 
            visible: false
        });
    }
}
    
const styles = StyleSheet.create({
  containerInput: {
    width: 340,
    height: 48,
    backgroundColor: "#ffffff",
    flex: 1,
    flexDirection: 'row',
    padding: 5,
    paddingTop: 13.5,
  },
  // design follow column
  blockInput1: {
    flex: 15,
    justifyContent: 'center',   
    alignItems: 'center',
  },
  blockInput2: {
    flex: 70,     
  },
  blockInput3: {
    flex: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon1: {
    width: 24.5,
    height: 23.5,
  }
})