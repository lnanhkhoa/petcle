export * from './navBar'; 
export * from './progressBar';
export * from './buttons';
export * from './texts';
export * from './sideMenu';
// export * from './camera';     
// export * from './qrcode';  
