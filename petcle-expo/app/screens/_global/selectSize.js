
import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet, ScrollView, Slider } from 'react-native';
import { Ionicons, SimpleLineIcons } from '../_global/Icons';
import { theme } from '../../config/themeProvider';
import {
    Screen, View, Text, Row, Icon, Button, Heading, Image,
    Caption, Subtitle, Title, ListView, ImageBackground, Tile, Divider, Html
} from '@shoutem/ui';
import { connectStyle } from '@shoutem/theme';
import { connect } from 'react-redux';
import { SizeListViewComponent } from '../../screens/_global/sizeListViewComponent';
import { routesName } from '../../config/navigation/routes';

export class SelectSizeComponent extends React.Component {
    static navigationOptions = ({ navigation }) => {
        return (
            {
                title: 'Select Size',
            });
    };

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: 'white', flex: 1 }} showsVerticalScrollIndicator={false} styleName='lg-gutter'>
                <Screen styleName='flexible space-between'>
                    <SizeListViewComponent />
                    <View>
                        <Button
                            styleName='elocker'
                            onPress={() => {
                                this.props.navigation.goBack();
                            }}
                        >
                            <Text>Done</Text>
                        </Button>
                    </View>
                </Screen>
            </ScrollView>
        );
    }
}
