
import React from 'react';
import {
    StyleSheet,
    View,
    Dimensions, Text, StatusBar, Animated, Platform, Alert,
} from 'react-native';
import _ from 'lodash';
import { UIConstants } from '../../config/appConstants';
import { Ionicons } from '../_global/Icons';
import { LinearGradient } from 'expo';
import { Button } from '@shoutem/ui';
import { setStatusBarStyle } from '../../helper/colorUtils';
import { theme } from '../../config/themeProvider';
import { inverseColorBrightnessForAmount } from '@shoutem/theme';
import { routesName } from '../../config/navigation/routes';


export class NavBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            width: undefined,
        };
    }

    _renderRight(headerRight, backgroundColor) {

        if (headerRight && headerRight.isScanQRCode != true) {
            return (
                <View style={styles.right}>{headerRight}</View>
            )
        }

        let windowWidth = Dimensions.get('window').width;
        const width = this.state.width
            ? (windowWidth - this.state.width) / 2
            : undefined;

        let renderRightContent = () => {
            if (headerRight && headerRight.isScanQRCode == true) {
                return <Button styleName='clear'
                    onPress={() => {
                        // this.props.navigation.navigate(routesName.global.CameraComponent, { name: routesName.global.CameraComponent });
                    }}>
                    <Ionicons
                        name='ios-qr-scanner-outline'
                        style={{ color: inverseColorBrightnessForAmount(backgroundColor, 100) }} />
                </Button>

            } else {
                // not drawer
            };
        };

        return (
            <View style={[width, styles.right]}>{renderRightContent()}</View>
        )
    }

    _renderLeft(headerLeft, backgroundColor) {
        if (headerLeft) {
            return (
                <View style={styles.left}>{headerLeft}</View>
            )
        }

        let windowWidth = Dimensions.get('window').width;
        const width = this.state.width
            ? (windowWidth - this.state.width) / 2
            : undefined;

        let renderLeftContent = () => {
            let index = _.findIndex(this.props.headerProps.scenes, { isActive: true });
            if (index > 0) {
                return <Button styleName='clear'
                    onPress={() => {
                        this.props.navigation.goBack();
                    }}>
                    <Ionicons
                        name='ios-arrow-back-outline'
                        style={{ color: inverseColorBrightnessForAmount(backgroundColor, 100) }} />
                </Button>
            }
            else {
                return <Button styleName='clear'
                    onPress={() => {
                        this.props.navigation.openDrawer();
                    }}>
                    <Ionicons
                        name='ios-menu-outline'
                        style={{ color: inverseColorBrightnessForAmount(backgroundColor, 100) }} />
                </Button>
            }
        };

        return (

            <View style={[width, styles.left]}>
                {renderLeftContent()}
            </View>
        )
    }

    _renderTitle(title, headerTitle, backgroundColor) {
        _onLayout = (e) => {
            this.setState({
                width: e.nativeEvent.layout.width
            })
        };
        if (headerTitle) {
            return (
                <View style={styles.title} onLayout={_onLayout}>{headerTitle}</View>);
        }
        return (
            <View style={styles.title} onLayout={_onLayout}>
                <Text style={{ color: inverseColorBrightnessForAmount(backgroundColor, 100) }}>{title}</Text>
            </View>
        )
    }

    render() {
        let options = this.props.headerProps.scene.descriptor.options;
        setStatusBarStyle(options.backgroundColor);
        let backgroundColor = options.backgroundColor ? options.backgroundColor : '#FFFFFF'

        return (
            <View style={[styles.container, { backgroundColor: backgroundColor}]}>
                {this._renderTitle(options.title, options.headerTitle, backgroundColor)}
                {this._renderLeft(options.headerLeft, backgroundColor)}
                {this._renderRight(options.headerRight, backgroundColor)}
            </View>
        );
    }
}

let styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: UIConstants.AppbarHeight,
        alignItems: 'center',
        paddingTop: UIConstants.StatusbarHeight,
        borderBottomColor: '#47315a',
        borderBottomWidth: 0.5,
    },
    left: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        justifyContent: 'center'
    },
    right: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        justifyContent: 'center'
    },
    title: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'center',
        alignItems: 'center',
    }
});