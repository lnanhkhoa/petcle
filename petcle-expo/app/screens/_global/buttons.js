/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Alert,
    StackActions,
    NavigationActions
} from 'react-native';
import { theme } from '../../config/themeProvider';
import { Google, Facebook } from 'expo';
import { FontAwesome } from '../_global/Icons';
import { Button,Text } from '@shoutem/ui';

/*=============================================
=            Button                           =
=============================================*/
//#region [Login Button]
export class ButtonLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            text: props.text ? props.text : 'CLICK HERE',
            icon: props.icon ? props.icon : 'emoticon-poop'
        };
        //emoticon-poop
    }
    
    _onLongPressButton() {
        Alert.alert('You long-pressed the button!')
    }
    render() {
        return (
            <Button onPress={this.props.onPress} styleName='login sm-padding lg-padding-left md-padding-right sm-gutter'>
                <FontAwesome styleName='xl-size white' name={this.state.icon} size={theme["shoutem.ui.Icon"].fontSize} />
                <Text>{this.state.text}</Text>
            </Button>
        );
    }
}
//#endregion
/*=========  End of Button  =================*/
