
import React, { Component } from 'react';
import { TouchableOpacity, Alert, StyleSheet, ScrollView, Slider } from 'react-native';
import { Ionicons } from '../_global/Icons';
import { theme } from '../../config/themeProvider';
import {
    Screen, View, Text, Row, Button, Heading,
    Caption, Subtitle, Title, Image, ListView, ImageBackground, Tile, Divider
} from '@shoutem/ui';
import { routesName } from '../../config/navigation/routes';
import DateTimePicker from 'react-native-modal-datetime-picker';
import _ from 'lodash';

export class FormCheckInOut extends React.Component {
    constructor(props) {
        super(props);
        var newtime = new Date(); 
        newtime.setMinutes(newtime.getMinutes() + 30);
        this.state = {
            startDateTimePickerVisible: false,
            endDateTimePickerVisible: false,
            datetime_checkin: new Date(),
            datetime_checkout: newtime,
            cal_time: '30 phút',
        }     
    }
    componentWillUpdate = (newprops, newstate) => {
        if(newprops.date_checkin != null){
            this.state.datetime_checkin = newprops.date_checkin;
            var cal_time = this.calculator_time(this.state.datetime_checkin ,this.state.datetime_checkout);
            this.state.cal_time = cal_time;
        }
        if(newprops.date_checkout != null){
            this.state.datetime_checkout = newprops.date_checkout;
            var cal_time = this.calculator_time(this.state.datetime_checkin ,this.state.datetime_checkout);
            this.state.cal_time = cal_time;
        }
    }

    formatTime = (date) => {
        hours = date.getHours();
        hours = (hours < 10) ? '0' + hours : hours;
        minutes = date.getMinutes();
        minutes = (minutes < 10) ? '0' + minutes : minutes;
        time_format = hours + 'h' + minutes;
        return time_format;
    }

    formatDate = (date) => {
        day = date.getDate();
        month = date.getMonth() + 1;
        weekday = ["Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat"];
        day_in_week = weekday[date.getDay()];
        day_format = day + '/' + month + '-' + day_in_week;
        return day_format;
    }

    calculator_time = (startTime, endTime) => {
        if(startTime == null && endTime == null){
            var startTime = "00:00:00";
            var endTime = new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
        }
        var timeDiff = Math.abs(endTime.getTime() - startTime.getTime());
        var hh = Math.floor(timeDiff / 1000 / 60 / 60);
        if(hh < 10 && hh > 0) {
            hh = '0' + hh;
        }
        timeDiff -= hh * 1000 * 60 * 60;
        var mm = Math.floor(timeDiff / 1000 / 60);
        if(mm < 10) {
            mm = '0' + mm;  
        }
        if(hh == '00'){
            time =  mm + ' phút';
        }else{
            time = hh + 'h' + mm ;
        }
        return time ;
    }

    showStartDateTimePicker = () => this.setState({ startDateTimePickerVisible: true });
    showEndDateTimePicker = () => this.setState({ endDateTimePickerVisible: true });
    hideStartDateTimePicker = () => this.setState({ startDateTimePickerVisible: false });
    hideEndDateTimePicker = () => this.setState({ endDateTimePickerVisible: false });

    handleStartDatePicked = (date) => {
        var cal_time = this.calculator_time(date ,this.state.datetime_checkout);
        this.setState(
            {
                datetime_checkin: date,
                cal_time : cal_time,
            }
        );
        this.props.onSelectChangeStartDate(date);    
        this.hideStartDateTimePicker();
    };

    handleEndDatePicked = (date) => {
        var cal_time = this.calculator_time(this.state.datetime_checkin ,date);
        this.setState(
            {
                datetime_checkout: date,
                cal_time : cal_time,
            }
        );
        this.props.onSelectChangeEndDate(date);     
        this.hideEndDateTimePicker();
    };

    componentWillMount() {
        const {navigation} = this.props;
        // date_checkin = navigation.getParam('date_checkin', null);
        // date_checkout = navigation.getParam('date_checkout', null);
        // this.setState({datetime_checkin : date_checkin} );
        // this.setState({datetime_checkout : date_checkout} );
    }
    
    render() {
        return (
            <View styleName='sm-margin-bottom horizontal paper'>
                <TouchableOpacity onPress={this.showStartDateTimePicker} disabled={this.props.touchDisable}>
                    <View style={theme.alignmentVariants[".middleLeft"]} styleName='sm-gutter lg-gutter-left'>
                        <Caption styleName='sm-gutter-bottom disclosure'>Check-in</Caption>
                        <Text>{this.formatDate(this.state.datetime_checkin)}</Text>
                        <Heading>{this.formatTime(this.state.datetime_checkin)}</Heading>
                    </View>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.startDateTimePickerVisible}
                    onConfirm={this.handleStartDatePicked}
                    onCancel={this.hideStartDateTimePicker}
                    maximumDate={this.state.datetime_checkout}
                    mode='datetime'
                />

                <View styleName='md-gutter-bottom flexible vertical' style={theme.alignmentVariants[".bottomCenter"]} >
                    <Ionicons styleName='xxxl-size bold' name='ios-arrow-round-forward-outline' />
                    <Caption>{this.state.cal_time}</Caption>
                </View>

                <TouchableOpacity onPress={this.showEndDateTimePicker} disabled={this.props.touchDisable}>
                    <View style={theme.alignmentVariants[".middleRight"]} styleName='sm-gutter lg-gutter-right'>
                        <Caption styleName='sm-gutter-bottom disclosure' >Check-out</Caption>
                        <Text >{this.formatDate(this.state.datetime_checkout)}</Text>
                        <Heading >{this.formatTime(this.state.datetime_checkout)}</Heading>
                    </View>
                </TouchableOpacity>
                <DateTimePicker
                    isVisible={this.state.endDateTimePickerVisible}
                    onConfirm={this.handleEndDatePicked}
                    onCancel={this.hideEndDateTimePicker}
                    minimumDate={this.state.datetime_checkin}   
                    mode='datetime'
                />
            </View>
        );
    }
}