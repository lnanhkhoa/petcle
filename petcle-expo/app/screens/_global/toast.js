// import React, { Component } from 'react';
// import { StyleSheet, View, Modal, Text, Button, Platform, Animated, Easing, Dimensions } from 'react-native';
// import PropTypes from 'prop-types';

// let windowWidth = Dimensions.get('window').width
// let windowHeight = Dimensions.get('window').height

// export class CustomToast extends Component {
//     constructor(props) {
//         super(props);
//         this.state = {
//             showToast: false,
//             animateOpacityValue: new Animated.Value(0),
//             animateMoveValue: new Animated.Value(0),
//             toastColor: 'green',
//             toastMessage: ''
//         }
//     }

//     componentWillUnmount() {
//         this.timerID && clearTimeout(this.timerID);
//     }

//     ShowToastFunction(message = "Custom React Native Toast", duration = 4000, type = undefined) {
//         this.setToastType(message, type);
//         this.setState({ showToast: true }, () => {
//             Animated.stagger(100, [
//                 Animated.timing(this.state.animateMoveValue, {
//                     toValue: 1,
//                     duration: 500,
//                     easing: Easing.cubic
//                 }),
//                 Animated.timing(this.state.animateOpacityValue, {
//                     toValue: 1,
//                     duration: 300
//                 })
//             ]).start(() => {
//                 this.HideToastFunction(duration)
//             })
//         });
//     }

//     HideToastFunction = (duration) => {
//         this.timerID = setTimeout(() => {
//             this.setState({ ShowToast: false });
//             Animated.stagger(300, [
//                 Animated.timing(this.state.animateOpacityValue, {
//                     toValue: 0,
//                     duration: 350
//                 }),
//                 Animated.timing(this.state.animateMoveValue, {
//                     toValue: 0,
//                     duration: 100,
//                     easing: Easing.linear
//                 })
//             ]).start(() => {
//                 clearTimeout(this.timerID);
//             })
//         }, duration);
//     }

//     setToastType = (message = 'Success!', type = 'success') => {
//         let color
//         if (type == 'error') color = 'red'
//         if (type == 'primary') color = '#2487DB'
//         if (type == 'warning') color = '#ec971f'
//         if (type == 'success') color = 'green'
//         if (type == 'noti') color = '#666666'
//         this.setState({ toastColor: color, toastMessage: message })
//     }

//     render() {
//         if (this.state.showToast) {
//             const animation = this.state.animateMoveValue.interpolate({
//                 inputRange: [0, .3, 1],
//                 outputRange: [-30, 0, 70]
//             })
//             let position = this.props.position == 'bottom' ? { bottom: animation } : { top: animation  };
//             return (
//                 <Animated.View style={[styles.animatedToastView, {
//                     ...position,
//                     opacity: this.state.animateOpacityValue,
//                     backgroundColor: this.state.toastColor
//                 }]}>
//                     <Text numberOfLines={1} style={[styles.ToastBoxInsideText, { color: this.props.textColor }]}>{this.state.toastMessage}</Text>
//                 </Animated.View >
//             );
//         }
//         else {
//             return null;
//         }
//     }
// }

// CustomToast.propTypes = {
//     backgroundColor: PropTypes.string,
//     position: PropTypes.oneOf([
//         'top',
//         'bottom'
//     ]),
//     textColor: PropTypes.string
// };

// CustomToast.defaultProps = {
//     backgroundColor: '#666666',
//     textColor: '#fff'
// }

// const styles = StyleSheet.create({

//     animatedToastView: {
//         marginHorizontal: 30,
//         paddingHorizontal: 25,
//         paddingVertical: 10,
//         borderRadius: 25,
//         zIndex: 9999,
//         position: 'absolute',
//         justifyContent: 'center',
//         alignSelf: 'center',
//     },

//     ToastBoxInsideText: {
//         fontSize: 15,
//         alignSelf: 'stretch',
//         textAlign: 'center'
//     }

// });


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */

import React, { Component } from 'react';
import {
    StyleSheet,
    Animated,
    Dimensions,
    TouchableHighlight
} from 'react-native';
import {
    Text,
    View,

} from '@shoutem/ui';
import { theme } from '../../config/themeProvider';
import { connect } from 'react-redux';
import {act_toast} from '../../actions/';

let windowWidth = Dimensions.get('window').width
let windowHeight = Dimensions.get('window').height


class _Toast extends React.Component {
    constructor(props) {
        super(props);
        this.animatedValue = new Animated.Value(0)
        this.animatedYValue = new Animated.Value(0)
        this.state = {
            modalShown: false,
        }
    }

    callToast() {

        if (this.state.modalShown) return

        this.setState({ modalShown: true })
        Animated.timing(
            this.animatedValue,
            {
                toValue: 1,
                duration: 350
            }).start(this.closeToast())
    }

    closeToast() {
        setTimeout(() => {
            this.setState({ modalShown: false })
            Animated.timing(
                this.animatedValue,
                {
                    toValue: 0,
                    duration: 350
                }).start()
        }, 2000)
    }

    callYToast() {
        if (this.state.modalShown) return
        this.setState({ modalShown: true })
        Animated.timing(
            this.animatedYValue,
            {
                toValue: 1,
                duration: 350
            }).start(this.closeYToast())
    }

    closeYToast() {
        setTimeout(() => {
            this.setState({ modalShown: false })
            Animated.timing(
                this.animatedYValue,
                {
                    toValue: 0,
                    duration: 350
                }).start()
            this.props.clear();
        }, 2000)
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps.message) {
            this.callYToast();
        }
    }

    render() {
        let animation = this.animatedYValue.interpolate({
            inputRange: [0, 1],
            outputRange: [-70, 0]
        })

        return (
            // <View styleName='full-screen' >
            //     <View style={styles.container}>
            //         <TouchableHighlight type="success" onPress={() => this.callToast('YOYOYO')} ><Text>success</Text></TouchableHighlight>
            //         <TouchableHighlight type="error" onPress={() => this.callToast('Error toast called!', 'error')} title="Something went wrong..." ><Text>error</Text></TouchableHighlight>
            //         <TouchableHighlight type="warning" onPress={() => this.callToast('Warning toast called!', 'warning')} ><Text>warning</Text></TouchableHighlight>
            //         <TouchableHighlight type="primary" onPress={() => this.callToast('Primary toast called!', 'primary')} ><Text>primary</Text></TouchableHighlight>
            //         <TouchableHighlight type="bottom" onPress={() => this.callXToast()} ><Text>bottom</Text></TouchableHighlight>
            //     </View>
            //     <Animated.View style={{ transform: [{ translateY: animation }], height: 70, backgroundColor: this.state.toastColor, position: 'absolute', left: 0, top: 0, right: 0, justifyContent: 'center' }}>
            //         <Text style={{ marginLeft: 10, color: 'white', fontSize: 16, fontWeight: 'bold' }}>
            //             {this.state.message}
            //         </Text>
            //     </Animated.View>
            //     <Animated.View style={{ transform: [{ translateX: this.animatedYValue }], height: 70, marginTop: windowHeight - 70, backgroundColor: 'green', position: 'absolute', left: 0, top: 0, width: windowWidth, justifyContent: 'center' }}>
            //         <Text style={{ marginLeft: 10, color: 'white', fontSize: 16, fontWeight: 'bold', textAlign: 'center' }}>Success!</Text>
            //     </Animated.View>
            // </View>
            <Animated.View style={
                {
                    transform: [{ translateY: animation }, { translateX: 0 }],
                    left: 0, top: 0, right: 0,
                    backgroundColor: this.props.color,
                    position: 'absolute',
                    justifyContent: 'center',
                    //marginHorizontal: 10,
                    //marginTop: 20,
                    //marginBottom: 0,
                    height: 30,
                    paddingHorizontal: 20,
                    paddingBottom: 10,
                    paddingTop: 20,
                    //borderRadius: 5,
                    zIndex: 9999,
                    alignSelf: 'center',
                }}>
                <Text style={{ color: 'white', fontSize: 12, fontWeight: 'bold', textAlign: 'center' }}>{this.props.message}</Text>
            </Animated.View>
        );
    }
}
const mapStateToProps = (state) => {
    let color = undefined;
    switch (state.toastReducers.type) {
        case 'error':
            color = theme._variables.accent
            break;
        case 'primary':
            color = theme._variables.primary
            break;
        case 'warning':
            color = theme._variables.warning
            break;
        case 'success':
            color = theme._variables.success
            break;
        case 'noti':
            color = theme._variables.inactive
            break;
        default:
            color = theme._variables.primary
            break;
    }
    let rs = {
        message: state.toastReducers.message ? state.toastReducers.message : undefined,
        type: state.toastReducers.type ? state.toastReducers.type : undefined,
        color
    };
    return rs;
}
const mapDispatchToProps = (dispatch) => {
    return {
        clear: (message, type) => {
            dispatch(act_toast.creators.clear());
        }
    }
}

export const Toast = connect(mapStateToProps, mapDispatchToProps)(_Toast);

