import React, { Component } from 'react';
import * as Icon from '@expo/vector-icons';
import { connectStyle } from '@shoutem/theme';
import { Animated } from 'react-native';

class fontAwesome extends React.Component {
    state = {}
    
    render() {
        return <Icon.FontAwesome {...this.props} />
    }
}
let FontAwesome = connectStyle('shoutem.ui.Icon')(fontAwesome);

class simpleLineIcons extends Component {
    state = {}
    render() {
        return <Icon.SimpleLineIcons {...this.props} />
    }
}
let SimpleLineIcons = connectStyle('shoutem.ui.Icon')(simpleLineIcons);

class ionicons extends Component {
    state = {}
    render() {
        //console.log(this.props.size)
        return <Icon.Ionicons {...this.props} />
    }
}
let Ionicons = connectStyle('shoutem.ui.Icon')(ionicons);

class materialIcons extends Component {
    state = {}
    render() {
        //console.log(this.props.size)
        return <Icon.MaterialIcons {...this.props} />
    }
}
let MaterialIcons = connectStyle('shoutem.ui.Icon')(materialIcons);

class materialCommunityIcons extends Component {
    state = {}
    render() {
        //console.log(this.props.size)
        return <Icon.MaterialCommunityIcons {...this.props} />
    }
}
let MaterialCommunityIcons = connectStyle('shoutem.ui.Icon')(materialCommunityIcons);


export {
    FontAwesome,
    SimpleLineIcons,
    Ionicons,
    MaterialIcons,
    MaterialCommunityIcons
}