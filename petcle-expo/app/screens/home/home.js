import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import {
  Title
} from '@shoutem/ui'

export class HomeComponent extends React.Component {
  static navigationOptions = ({ navigation }) => {
    let renderTitle = params => {
      return (
        <TouchableOpacity
          style={{ alignItems: "center", justifyContent: "center" }}
          onPress={() => {
          }}
        >
          <Title>Agrismart</Title>
        </TouchableOpacity>
      );
    };
    let centerTitle = renderTitle();
    return {
      headerTitle: centerTitle
    };
  };
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
