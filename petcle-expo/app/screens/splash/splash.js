import React, {Component} from 'react';
import {StyleSheet, Image, Dimensions} from 'react-native';
import {Wallpaper, FormLogin, ButtonSubmit} from '../components/'

export class SplashScreen extends Component {
  render() {
    return (
      <Wallpaper background={require('../../assets/images/splash.png')}>
      </Wallpaper>
    );
  }
}

const styles = StyleSheet.create({
  form: {
    height: 100
  }
})
