import React, { Component } from 'react';
import { StyleSheet, FlatList} from 'react-native';
import { Row, Icon, View, Text, Subtitle  } from '@shoutem/ui';

const datalist = {
  main: {
    children: [{
      key: '0',
      mainTitle: true,
      title: "Main Settings",
    },{
      key: '1',
      title: 'Snack Call',
      note: 'Default Recording',
      icon: 'arrow-right'
    },{
      key: '2',
      title: 'Video Quality',
      note: '720p',
      icon: 'arrow-right'
    },{
      key: '3',
      title: 'Night Vision',
      note: 'Auto',
      icon: 'arrow-right'
    },{
      key: '4',
      title: 'Barking Alert',
      note: 'Off',
      icon: 'arrow-right'
    },{
      key: '5',
      title: 'Volume',
      note: '',
      icon: 'arrow-right'
    },{
      key: '6',
      title: 'Timezone Setting',
      note: '',
      icon: 'arrow-right'
    },{
      key: '7',
      title: 'Camera On/Off',
      note: 'On',
      icon: 'arrow-right'
    },{
      key: '8',
      title: 'Reset Wifi',
      note: '',
      icon: 'arrow-right'
    },{
      key: '9',
      mainTitle: true,
      title: "Getting Started"
    },{
      key: '10',
      title: 'Watch the Training Video',
      hyperlinkTitle: true,
      note: '',
      icon: 'arrow-right'
    }]
  },
}


export class SettingsScreen extends Component {
  constructor(props){
    super(props);
    this.state = {

    }
  }

  renderRow = ({item}) => {
    const {mainTitle, hyperlinkTitle,  title, note } = item
    if(!!mainTitle){
      return(
        <Row styleName="s-small"
          style={{backgroundColor: 'transparent'}}>
          <Text>{title}</Text>
        </Row>
      );
    }

    if(!!hyperlinkTitle){
      return (
        <Row styleName="small">
            <Text style={styleShoutem.hyperText}>
              {title}
            </Text>
        </Row>
      );
    }
    
    return(
      <Row styleName="small">
        <View styleName="space-between horizontal">
          <Subtitle>{title}</Subtitle>
          <Text numberOfLines={1}>{note}</Text>
        </View>
        <Icon styleName="disclosure" name="right-arrow" />
      </Row>
    );
  }

  render() {
    return (
      <View styleName="flexible h-center v-center"
        style={{backgroundColor: '#f2f2f2'}}
      >
        {/* <Subtitle> Main Settings </Subtitle> */}
        <FlatList 
          style={styles.flatlist}
          data={datalist.main.children}
          renderItem={this.renderRow}
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  flatlist: {
    marginTop: 20
  }
})

const styleShoutem = {
  hyperText: {
    color: "#2980b9"
  }
}